#let to-string(content) = {
  if content.has("text") {
    content.text
  } else if content.has("children") {
    content.children.map(to-string).join("")
  } else if content.has("body") {
    to-string(content.body)
  } else if content == [ ] {
    " "
  }
}

#let project(
  type-or-title: "Bundesvorstandssitzung", authors: (),
  start-date: none, end-date: none,
  place: none, place-modifier: "in",
  body
) = {
  let title = [ #(type-or-title) #if place != none [#(place-modifier) #(place)] ]

  // Set the document's basic properties.
  set document(author: authors, title: title)
  set page(numbering: "1", number-align: end, header: [
    #align(right)[#image("assets/logo-wortmarke.min.svg", width: 15%)]
  ])
  set text(font: "Source Sans Pro", weight: 400, lang: "de")

  //   set heading(numbering: "1a")
  set heading(numbering: none)
  show heading: it => locate(loc => {
    // Find out the final number of the heading counter.
    let levels = counter(heading).at(loc)

    set text(font: "Amaranth")

    let s = to-string(it.body)

    if s == "Anhänge" [
        #pagebreak()
        #block(above: 24pt, below: 12pt, [ #numbering("1", ..levels) #it.body ])
    ] else if (it.level == 1 or it.level == 2) and s.starts-with("Anhang") [
        #block(above: 24pt, below: 12pt, [
            #if it.numbering != none [ #numbering("1 A1", ..levels) #h(6pt, weak: true) ]
            #it.body
        ]
    )] else if it.level == 1 or it.level == 2 [ #block(above: 24pt, below: 12pt, [
      #if it.numbering != none [ #numbering("1a", ..levels) #h(6pt, weak: true) ]
      #it.body
    ])] else [
      #block(above: 18pt, below: 9pt, it.body)
    ]
  })

  // Title row.
  align(center)[
    #block(text(weight: 700, 1.75em, font: "Amaranth", title))
    #v(2.5em, weak: true)
    #if start-date != none and end-date != none [
      vom #(start-date) bis zum #(end-date)
    ] else if start-date != none [am #(start-date)]
  ]

  // Main body.
  set par(justify: true)

  body
}
