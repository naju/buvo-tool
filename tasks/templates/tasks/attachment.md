{% load i18n template_helpers %}
{% if appendices %}

# {% trans "Anhänge" %}

{% for appendix_name, attachments in appendices %}
## {% trans "Anhang" %} {{appendix_name}}

{% for attachment in attachments %}{% if standalone %}
![{{attachment.caption}}]({{attachment.file.path | relative_to_typst_root}} "{{attachment.caption}}"){ width=100% }{% else %}
![{{attachment.caption}}]({{attachment.file.url}} "{{attachment.caption}}"){ width=100% }{% endif %}
{# ![{{attachment.caption}}]({{attachment.file.path}} "{{attachment.caption}}"){ width=100% }{% else %} #}
{% comment %} the standalone entry is relative to the typst root (the project dir) {% endcomment %}

{% endfor %}
{% endfor %}
{% endif %}
