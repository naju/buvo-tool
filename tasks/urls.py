from django.urls import path
from . import views as v

app_name = "tasks"

urlpatterns = [
    path('protocol/<int:pk>/', v.ProtocolStatic.as_view(), name='protocol-html'),
    path('protocol/<int:pk>.html', v.ProtocolStatic.as_view(), name='protocol-html2'),
    path('protocol/<int:pk>.md', v.ProtocolMd.as_view(), name='protocol-md'),
    path('protocol/<int:pk>/mail-lv/', v.SendLvEmailView.as_view(), name='lv-mail-intermediate'),
    path('protocol/<int:pk>/edit/', v.ProtocolEditView.as_view(),
        name='protocol-edit', kwargs={'room_prefix': 'protocol-'}),
    path(
        'protocol/create/<str:signed_room_name>/', v.ProtocolCreateView.as_view(),
        name='protocol-create-from-editor', kwargs={
            'room_prefix': 'new-protocol-', 'edit_room_prefix': 'protocol-'
        }),
    path(
        'protocol/create/', v.ProtocolCreateRedirectView.as_view(),
        name='protocol-create-redirect', kwargs={'room_prefix': 'new-protocol-'}),
]
