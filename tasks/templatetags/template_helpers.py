import logging
from pathlib import Path

from django import template
from django.conf import settings
from django.template.base import VariableDoesNotExist

from tasks import helpers as h

logger = logging.getLogger('buvo')
register = template.Library()

@register.simple_tag
def wiki(page: str):
    parts = page.split(':')
    parts = [part.lower() for part in parts]
    parts = [part.replace(' ', '_') for part in parts]
    return settings.WIKI + "/".join(parts)

register.filter(name='json_array_to_str', filter_func=h.json_array_to_str)
register.simple_tag(h.verbose_name)

@register.filter(name='relative_to_typst_root')
def relative_to_typst_root(value: str) -> str:
    return "/" + str(Path(value).relative_to(settings.TYPST_ROOT))

class DestructuringNode(template.Node):
    """ Code of this node is partly borrowed from the built-in ``{% with %}``-Node. """
    def __init__(self, nodelist, context_dict):
        self.nodelist = nodelist
        self.context_dict = template.Variable(context_dict)

    def __repr__(self):
        return '<%s>' % self.__class__.__name__

    def render(self, context):
        try:
            context_obj = self.context_dict.resolve(context)
        except VariableDoesNotExist:
            # this tag should work like built-in tags do: do nothing if the variable does not exist
            logger.warning(
                'destructure: variable "%s" does not exist in context.',
                self.context_dict)
            context_obj = dict()
        # best effort. use with care.
        if isinstance(context_obj, dict):
            pass
        elif hasattr(context_obj, '__dict__'):
            context_obj = context_obj.__dict__
        else:
            raise AttributeError('the context object you put in your destructure-tag '
                                 'must be a dict or support conversion via __dict__.')
        with context.push(**context_obj):
            return self.nodelist.render(context)

@register.tag('destructure')
def with_destructured_object(parser, token):
    """
    The ``destructure`` works like the ``{% with %}``-tag except that it takes one object
    or a dict as an argument. All of the attributes of that object of dict will be available
    directly inbetween ``{% destructure obj %}`` and ``{% enddestructure %}``. If you pass
    an object, it must support an ``obj.__dict__`` parameter. The object could for example
    be a ``Model`` instance.::

        # in the view's get_context_data():
        context['test'] = { 'var_a': 'some string' }

        # in the template:
        {% destructure test %}
        <p>A: {{var_a}}</p>
        {% enddestructure %}

    This is especially useful when you need to access a large number of attributes while
    using ``{% blocktrans %}``.
    """
    nodelist = parser.parse(('enddestructure',))
    _, context_dict = token.split_contents() # get the tag argument
    parser.delete_first_token() # remove the tag arg
    return DestructuringNode(nodelist, context_dict)
