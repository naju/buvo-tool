import logging
from datetime import timedelta

from django.contrib import admin, messages
from django.contrib.admin.exceptions import DisallowedModelAdminToField
from django.contrib.admin.options import TO_FIELD_VAR
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.contrib.admin.utils import unquote
from django.core.exceptions import ValidationError
from django.core.signing import Signer
from django.db.models import F, Q, QuerySet
from django.http.request import HttpRequest
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from . import forms as f
from . import models as m

logger = logging.getLogger("buvo")
signer = Signer()

class ActionButtonAdmin(admin.ModelAdmin):
    def admin_self_url(self, request, object_id):
        opts = self.model._meta
        preserved_filters = self.get_preserved_filters(request)
        redirect_url = reverse(
            'admin:%s_%s_change' % (opts.app_label, opts.model_name),
            args=(object_id,),
            current_app=self.admin_site.name)
        redirect_url = add_preserved_filters({
            'preserved_filters': preserved_filters,
            'opts': opts}, redirect_url)
        return redirect_url

    def change_view(self, request, object_id, form_url='', extra_context=None):
        # from django. this will be repeated afterwards but
        to_field = request.POST.get(TO_FIELD_VAR, request.GET.get(TO_FIELD_VAR))
        if to_field and not self.to_field_allowed(request, to_field):
            raise DisallowedModelAdminToField("The field %s cannot be referenced." % to_field)
        to_field = to_field or 'pk'

        extra_context = extra_context or dict()

        actions = self.get_actions(request)
        for func, name, _ in actions.values():
            if "_" + name in request.POST:
                qs = self.model.objects.filter(**{to_field: unquote(object_id)})
                if (response := func(self, request, qs)):
                    return response
                redirect_url = self.admin_self_url(request, unquote(object_id))
                return HttpResponseRedirect(redirect_url)

        extra_context['action_choices'] = [
            ('_' + name, desc) \
            for name, desc in self.get_action_choices(request) \
            if name and name not in ['delete_selected']]

        return super().change_view(
            request, object_id, form_url=form_url, extra_context=extra_context)


class ListFilterWithDefault(admin.SimpleListFilter):
    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({self.parameter_name: lookup}, []),
                'display': title}


class AttachmentInline(admin.StackedInline):
    model = m.Attachment
    extra = 1
    fields = [
        ("file", "caption"),
        ("appendix_name", "position", "internal")]

    def has_module_permission(self, request: HttpRequest) -> bool:
        return request.user.is_staff


class TaskInline(admin.StackedInline):
    model = m.Task
    form = f.TaskForm
    extra = 0
    fields = [
        "text",
        ("done", "due_date"),
        "assignees"]

    def has_add_permission(self, request, obj) -> False:
        return False


class ResolutionInline(admin.StackedInline):
    model = m.Resolution
    form = f.ResolutionForm
    extra = 0
    fields = [
        "text",
        ("done", "due_date"),
        ("yes", "no", "abstention"),
        "assignees"]

    def has_add_permission(self, request, obj) -> False:
        return False


class ProtocolAdmin(ActionButtonAdmin):
    change_form_template = "admin/tasks/protocol/change_form.html"
    change_list_template = "admin/tasks/protocol/change_list.html"
    form = f.ProtocolForm
    actions = ["make_downloads", "send_mail_to_lv", "extract_todos", "extract_resolutions"]
    readonly_fields = ['downloads_are_latest']
    list_display = ['__str__', 'start_date', 'end_date']
    search_fields = ['search_dump']
    inlines = [TaskInline, ResolutionInline, AttachmentInline]
    fields = [
        'protocol_md',
        ('session_type', 'location'),
        ('start_date', 'end_date'),
        'attendees', 'moderation', 'recorder',
        'downloads_are_latest']

    def get_queryset(self, request) -> QuerySet[m.Protocol]:
        return super().get_queryset(request).order_by('-start_date', '-end_date', 'id')

    def save_model(self, request, obj: m.Protocol, form, change):
        # obj.make_html() # commented out because this was moved to save_related()
        # obj.downloads_are_latest = False
        super().save_model(request, obj, form, change)
        # if "protocol_md" in form.changed_data:
        # delete in all cases as all meat fields need to be updated if the editor is opened again
        # TODO: this may not be safe when one person saves, exits and edits the metadata while
        #       other persons are still in the room!
        m.CrdtBlob.objects.filter(room_name=f"protocol-{obj.pk}").delete()

    def save_related(self, request, form, formsets, change) -> None:
        super().save_related(request, form, formsets, change)
        # the object needs to be saved twice so we can generate
        # the html after the appendices have been saved...
        obj: m.Protocol = form.instance
        obj.make_html()
        obj.save()

    @admin.action(description=_("Mail an LV versenden"), permissions=['mail'])
    def send_mail_to_lv(self, request, queryset: QuerySet[m.Protocol]):
        if queryset.count() != 1:
            self.message_user(
                request, level=messages.ERROR,
                message=_(
                    "Diese Aktion kann nur mit exakt einem "
                    "einzelnen Protokoll durchgeführt werden."))
            return

        obj = queryset.only('pk').first()
        return HttpResponseRedirect(reverse('tasks:lv-mail-intermediate', kwargs={'pk': obj.pk}))

    def has_mail_permission(self, request: HttpRequest):
        objid = request.resolver_match.kwargs.get('object_id', None)
        if objid is None:
            return False
        obj = self.get_object(request, unquote(objid))
        return obj.protocol_extern_pdf is not None and obj.downloads_are_latest

    @admin.action(description=_("Aufgaben extrahieren"))
    def extract_todos(self, request, queryset):
        try:
            for protocol in queryset:
                protocol.extract_todos()
            self.message_user(
                request, level=messages.SUCCESS,
                message=_("Die Aufgaben wurden erfolgreich extrahiert."))
        except ValidationError as e:
            for error in e.error_list:
                self.message_user(
                    request, level=messages.ERROR,
                    message=_("Fehler beim Extrahieren der Aufgaben: %s") % (error.message,))

    @admin.action(description=_("Beschlüsse extrahieren"))
    def extract_resolutions(self, request, queryset):
        try:
            for protocol in queryset:
                protocol.extract_resolutions()
            self.message_user(
                request, level=messages.SUCCESS,
                message=_("Die Beschlüsse wurden erfolgreich extrahiert."))
        except ValidationError as e:
            for error in e.error_list:
                self.message_user(
                    request, level=messages.ERROR,
                    message=_("Fehler beim Extrahieren der Beschlüsse: %s") % (error.message,))

    @admin.action(description=_("ODT- und PDF-Protokolle erstellen"))
    def make_downloads(self, request, queryset):
        for protocol in queryset:
            protocol.make_downloads()
        self.message_user(
            request, level=messages.SUCCESS,
            message=_("Die Protokolle wurden erfolgreich erstellt."))

admin.site.register(m.Protocol, ProtocolAdmin)


class TaskDoneFilter(ListFilterWithDefault):
    title = _("Erledigt")

    parameter_name = 'done'

    def lookups(self, request, model_admin):
        return [
            (None,      _("Nicht erledigt")),
            ('done',    _("Erledigt")),
            ('all',     _("Alle")),
        ]

    def queryset(self, request, queryset: QuerySet[m.Item]):
        if self.value() == 'done':
            return queryset.filter(done=True)

        if self.value() == 'all':
            return queryset

        return queryset.filter(done=False)


class TaskDateFilter(admin.SimpleListFilter):
    title = _("Datum")

    parameter_name = 'due_date'

    def lookups(self, request, model_admin):
        return [
            ('dead',    _("Deadline verstrichen")),
            ('7days',   _("Nächste 7 Tage")),
            ('nodate',  _("Keine Deadline")),
        ]

    def queryset(self, request, queryset: QuerySet[m.Item]):
        if self.value() == 'dead':
            return queryset.filter(due_date__lt=now().date())

        if self.value() == '7days':
            jetzt = now().date()
            return queryset.filter(due_date__lt=jetzt + timedelta(days=7), due_date__gte=jetzt)

        if self.value() == 'nodate':
            return queryset.filter(due_date=None)


class ItemAdmin(admin.ModelAdmin):
    change_form_template = "admin/tasks/item/change_form.html"
    readonly_fields = ["creation_date"]
    list_display_links = ["status", "text"]
    list_display = ["done", "text", "status", "due_date"]
    list_editable = ["done"]
    search_fields = ["text"]

    def get_queryset(self, request) -> QuerySet[m.Item]:
        return super().get_queryset(request).order_by('-due_date', 'done')


class TaskAdmin(ItemAdmin):
    form = f.TaskForm
    list_filter = [TaskDateFilter, TaskDoneFilter]
    fields = [
        "text",
        ("due_date", "creation_date"),
        "assignees",
        "protocol",
        "done"]

admin.site.register(m.Task, TaskAdmin)


class OnlyPassedResolutionsFilter(ListFilterWithDefault):
    title = _("Angenommen")

    parameter_name = 'yes'

    def lookups(self, request, model_admin):
        return [
            (None,     _("Angenommen")),
            ('rej',    _("Abgelehnt")),
            ('all',    _("Alle")),
        ]

    def queryset(self, request, queryset: QuerySet[m.Item]):
        if self.value() == 'rej':
            return queryset.filter(yes__isnull=False, no__gte=F('yes'))

        if self.value() == 'all':
            return queryset

        return queryset.filter(Q(yes__isnull=True) | Q(yes__gt=F('no')))


class ResolutionAdmin(ItemAdmin):
    form = f.ResolutionForm
    list_filter = [OnlyPassedResolutionsFilter, TaskDateFilter, TaskDoneFilter]
    fields = [
        "text",
        ("due_date", "creation_date"),
        "assignees",
        "protocol",
        ("yes", "no", "abstention"),
        "done"]

admin.site.register(m.Resolution, ResolutionAdmin)


class BlobAdmin(admin.ModelAdmin):
    fields = ["room_name"]
    readonly_fields = ["timestamp"]
    list_display = ["room_name", "timestamp"]

admin.site.register(m.CrdtBlob, BlobAdmin)
