import base64
import json
import uuid
from typing import Any, Dict

from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin.templatetags.admin_urls import admin_urlname
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.core.signing import Signer
from django.db import models
from django.forms.utils import ErrorDict
from django.http import HttpResponse
from django.http.request import HttpHeaders, HttpRequest
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import Context, Engine
from django.urls import reverse
from django.urls.base import get_script_prefix
from django.utils.translation import gettext_lazy as _
from django.views import View, generic
from django.views.generic import detail

from . import forms as f
from . import helpers as h
from . import models as m

engine = Engine.get_default()
signer = Signer()


class ProtocolStatic(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        html = get_object_or_404(m.Protocol, pk=pk)
        return HttpResponse(html.protocol_html, content_type='text/html')


class ProtocolMd(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        html = get_object_or_404(m.Protocol, pk=pk)
        return HttpResponse(html.md_editable, content_type='text/x-markdown')


class AdminObjectMixin(detail.SingleObjectMixin):
    subtitle: str

    def dispatch(self, request, *args, **kwargs):
        self.object: models.Model = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def get_admin_context_data(self):
        title = getattr(self.object, 'title', None)
        title = title or _('%s erstellen') % (self.object._meta.verbose_name,)
        return {
            'available_apps': admin.site.get_app_list(self.request),
            'is_nav_sidebar_enabled': True,
            'opts': self.object._meta,
            'site_header': admin.site.site_header,
            'site_title': admin.site.site_title,
            'title': title,
            'subtitle': self.subtitle
        }

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context.update(self.get_admin_context_data())
        return context


class SendLvEmailView(
    LoginRequiredMixin, AdminObjectMixin,
    detail.SingleObjectTemplateResponseMixin,
    generic.FormView
):
    model = m.Protocol
    context_object_name = 'protocol'
    template_name = 'tasks/lv_mail_intermediate.html'
    form_class = f.ProtocolMailForm
    success_message = _('Die E-Mail wurde versendet.')
    error_message = _('Die E-Mail konnte nicht versendet werden.')
    fields = ['from_email', 'to', 'reply_to', 'subject', 'body']
    subtitle = _('Protokoll an LV e-mailen')

    def get_success_url(self) -> str:
        return reverse(admin_urlname(self.object._meta, 'change'), args=[self.object.pk])

    def get_initial(self) -> Dict[str, Any]:
        initial = super().get_initial()
        lv_mail_tpl = engine.get_template('tasks/protokolle_versenden_lv.html')
        lv_mail_ctx = Context({'protocol': self.object})
        lv_mail = getattr(settings, 'LV_MAIL', '')
        return {
            **initial,
            'from_email': settings.DEFAULT_FROM_EMAIL,
            'to': lv_mail,
            'subject': h.render_template_node(lv_mail_tpl, lv_mail_ctx, 'subject'),
            'body': h.render_template_node(lv_mail_tpl, lv_mail_ctx, 'body'),
        }

    def get_form_kwargs(self) -> Dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs['attachment_name'] = self.object.protocol_extern_pdf.name
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.get_admin_context_data())
        return context

    def form_valid(self, form) -> HttpResponse:
        data = form.cleaned_data
        email = EmailMessage(
            data.pop('subject'), data.pop('body'),
            bcc=[settings.DEFAULT_FROM_EMAIL], **data)
        if 'smtp.EmailBackend' in getattr(settings, 'EMAIL_BACKEND', ''):
            email.attach_file(self.object.protocol_extern_pdf.path, 'application/pdf')
        if email.send(fail_silently=True):
            messages.success(self.request, self.success_message)
            # self.message_user(request, _('Die E-Mail wurde versendet.'), level=messages.SUCCESS)
            return HttpResponseRedirect(self.get_success_url())
        messages.error(self.request, self.error_message)
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, self.error_message)
        return super().form_invalid(form)


class ProtocolCreateRedirectView(LoginRequiredMixin, generic.RedirectView):
    permanent = False
    pattern_name = 'tasks:protocol-create-from-editor'

    def get_redirect_url(self, *args, **kwargs):
        room_id = uuid.uuid4()
        kwargs['signed_room_name'] = signer.sign(self.kwargs.get('room_prefix') + str(room_id))
        return super().get_redirect_url(*args, **kwargs)


class ProtocolEditView(
    LoginRequiredMixin, AdminObjectMixin,
    detail.SingleObjectTemplateResponseMixin,
    generic.TemplateView
):
    model = m.Protocol
    subtitle = _('Protokoll bearbeiten')
    context_object_name = 'protocol'
    template_name = 'tasks/edit_protocol_full.html'
    success_message = _('Das Protokoll wurde gespeichert.')
    error_message = _('Das Protokoll konnte nicht gespeichert werden.')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request: HttpRequest, *args, **kwargs):
        json_data = json.loads(request.body.decode('utf-8'))
        self.object.protocol_md = json_data.get('protocolMd', None)
        try:
            self.object.full_clean()
            return self.data_valid(json_data)
        except ValidationError as e:
            errors = h.error_to_error_dict(e.error_dict)
            return self.data_invalid(json_data, errors)

    @property
    def room_name(self):
        return self.kwargs.get('room_prefix') + str(self.object.pk)

    @property
    def signed_room_name(self):
        return signer.sign(self.room_name)

    def get_success_url(self) -> str:
        return reverse(admin_urlname(self.object._meta, 'change'), args=[self.object.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        request = self.request
        ws_url = 'wss://' if request.is_secure() else 'ws://'
        ws_url += request.get_host() + get_script_prefix()
        ws_url += 'ws' if ws_url.endswith('/') else '/ws'
        latest_blob = m.CrdtBlob.objects.filter(room_name=self.room_name).first()
        persisted_doc = latest_blob and base64.b64encode(latest_blob.blob).decode('utf-8')
        context.update({
            'editor_settings': {
                'user': request.user.username,
                'room': self.room_name,
                'password': self.signed_room_name,
                'signaling': ws_url + '/signaling/',
                'persistance': ws_url + '/persisting/',
                'persistedDoc': persisted_doc,
                'csrfHeaderName': HttpHeaders.parse_header_name(settings.CSRF_HEADER_NAME),
                'turnServer': settings.TURN_SERVER,
                'turnUser': settings.TURN_USER,
                'turnPassword': settings.TURN_PASSWORD,
                'stunServer': settings.STUN_SERVER,
            }
        })
        return context

    def get_response_data(self, data):
        return {
            'type': 'success',
            'exitDestinationURL': self.get_success_url(),
        }

    def save_blob(self, blob: bytes):
        m.CrdtBlob.objects.update_or_create(
                room_name=self.room_name,
                defaults={'blob': blob})

    def data_valid(self, data) -> HttpResponse:
        if self.object.pk is None:
            self.object.save()
        self.object.make_html()
        self.object.save()
        if 'blob' in data:
            blob = base64.b64decode(data['blob'])
            self.save_blob(blob)
        return HttpResponse(json.dumps(self.get_response_data(data)), status=200)

    def data_invalid(self, data, errors: ErrorDict) -> HttpResponse:
        response_data = {
            'type': 'error',
            'errors': errors.get_json_data(),
            'errorsHtml': errors.as_ul(),
        }
        return HttpResponse(json.dumps(response_data), status=200)


class ProtocolCreateView(ProtocolEditView):
    def get_object(self, queryset=None):
        return self.model()

    @property
    def room_name(self):
        return signer.unsign(self.signed_room_name)

    @property
    def room_name_on_created(self):
        return self.kwargs.get('edit_room_prefix') + str(self.object.pk)

    @property
    def signed_room_name(self):
        return self.kwargs.get('signed_room_name')

    def get_creation_success_url(self) -> str:
        return reverse('tasks:protocol-edit', kwargs={'pk': self.object.pk})

    def get_response_data(self, data):
        data = super().get_response_data(data)
        data['creationSuccessURL'] = self.get_creation_success_url()
        return data

    def save_blob(self, blob: bytes):
        m.CrdtBlob.objects.update_or_create(
                room_name=self.room_name_on_created,
                defaults={'blob': blob})
        # delete the old blob as the new blob has been saved
        m.CrdtBlob.objects.filter(room_name=self.room_name)\
            .delete()
