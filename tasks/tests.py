import re
from datetime import date

from django.test import SimpleTestCase

from . import helpers

todo_1 = "Molestiae aspernatur 2021-12-21 corporis placeat iste qui."
todo_2 = "Consequatur voluptatum iusto bis:22.12.2021 minus possimus ut quas quia id."

todo_txt = f"""k1: v1, v2, v3

**k2:** v1


_k3:_ asdf
* Id iusto adipisci minima laborum voluptates quos eius.
* Iusto at reiciendis vitae.
* *ACT:* {todo_1}
    - __ACT__ {todo_2}
"""

class HelperTests(SimpleTestCase):
    def test_extract_todo(self):
        todos = helpers.extract_todos(todo_txt, helpers.todo)
        self.assertIn(todo_1, todos)
        self.assertIn(todo_2, todos)

    def test_extract_assignees(self):
        a1 = "aspernatur"
        a2 = "placeat"
        assignees = helpers.extract_assignees(todo_1, [a1, a2])
        self.assertIn(a1, assignees)
        self.assertIn(a2, assignees)

    def test_extract_due_date(self):
        due = date(year=2021, month=12, day=21)
        due_ex = helpers.extract_date(todo_1)
        self.assertEqual(due, due_ex)
        due = date(year=2021, month=12, day=22)
        due_ex = helpers.extract_date(todo_2)
        self.assertEqual(due, due_ex)

    def test_header_meta_extraction(self):
        meta, md = helpers.extract_header_meta(todo_txt)
        self.assertIn('k1', meta)
        self.assertEqual(len(meta['k1']), 3)
        self.assertIn('k2', meta)
        self.assertEqual(len(meta['k2']), 1)
        self.assertIn('k3', meta)
        match = re.match(r'^\* Id iusto', md)
        self.assertIsNotNone(match)

    def test_field_nomalization(self):
        meta, _ = helpers.extract_header_meta(todo_txt)
        field_map = {(re.compile(r'^k1$'), 'kx')}
        meta, unknwon = helpers.normalize_known_meta_keys(meta, field_map)
        self.assertIn('kx', meta)
        self.assertIn('k2', unknwon)
        self.assertIn('k3', unknwon)
        self.assertEqual(len(meta), 1)
        self.assertEqual(len(unknwon), 2)
