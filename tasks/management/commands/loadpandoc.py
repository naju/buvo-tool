from django.core.management.base import BaseCommand
from argparse import ArgumentParser
from pypandoc.pandoc_download import download_pandoc

class Command(BaseCommand):
    help = "Download pandoc to convert protocols."

    def handle(self, *args, **kwargs):
        download_pandoc()
