from django.urls import path
from django.conf import settings
import logging
from . import consumers

logger = logging.getLogger('buvo')

prefix = getattr(settings, 'FORCE_SCRIPT_NAME', '').strip('/')
ws_url = prefix + '/' if prefix else '' + 'ws/'
# TODO: script prefix does not work yet. this has been solved using nginx config.

websocket_urlpatterns = [
    path(ws_url + 'signaling/',
         consumers.SignalingConsumer.as_asgi(), name="signaling"),
    path(ws_url + 'persisting/',
         consumers.CrdtPersistanceConsumer.as_asgi(), name="persisting"),
]
