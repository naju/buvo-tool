from os.path import basename

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Field, Fieldset, Layout
from django import forms
from django.core.files.uploadedfile import UploadedFile
from django.utils.translation import gettext_lazy as _

from . import models as m
from .helpers import split_txt_list, verbose_name, json_array_to_str


class JSONListField(forms.JSONField):
    def prepare_value(self, value):
        if isinstance(value, forms.fields.InvalidJSONInput):
            return value
        if isinstance(value, list):
            return json_array_to_str(value)
        return str(value)

    def to_python(self, value):
        return split_txt_list(value)


class ProtocolForm(forms.ModelForm):
    class Meta:
        model = m.Protocol
        exclude = [
            'protocol_html', 'search_dump', 'crdt_blob', 'protocol_intern_odt',
            'protocol_intern_pdf', 'protocol_extern_odt', 'protocol_extern_pdf']

    attendees = JSONListField(
            label=verbose_name(m.Protocol, 'attendees'),
            widget=forms.Textarea({'rows': 3}), required=False,
            help_text=_("Komma-separierte Liste mit Zusatzinfos in Klammern"))
    recorder = JSONListField(
            label=verbose_name(m.Protocol, 'recorder'),
            widget=forms.Textarea({'rows': 3}), required=False,
            help_text=_("Komma-separierte Liste"))
    moderation = JSONListField(
            label=verbose_name(m.Protocol, 'moderation'),
            widget=forms.Textarea({'rows': 3}), required=False,
            help_text=_("Komma-separierte Liste"))
    protocol_md = forms.FileField(
            label=verbose_name(m.Protocol, 'protocol_md'))

    def clean_protocol_md(self):
        protocol_file = self.cleaned_data['protocol_md']
        if isinstance(protocol_file, UploadedFile):
            text = protocol_file.read().decode('utf-8')
            return text
        return protocol_file


class SpanWidget(forms.TextInput):
    template_name = 'forms/span_widget.html'


class ItemForm(forms.ModelForm):
    text = forms.CharField(
        label=verbose_name(m.Item, 'text'),
        widget=forms.Textarea({'rows': 4, 'cols': 55}))
    assignees = JSONListField(
        required=False,
        label=verbose_name(m.Item, 'assignees'),
        help_text=_("Komma-separierte Liste"),
        widget=forms.Textarea({'rows': 2, 'cols': 55}))

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     if self.instance.protocol is not None:
    #         self.fields['text'].widget = SpanWidget()
    #         self.fields['text'].disabled = True


class ResolutionForm(ItemForm):
    class Meta:
        model = m.Resolution
        fields = '__all__'

    def __init__(self, *args, instance=None, **kwargs):
        super().__init__(*args, instance=instance, **kwargs)
        if instance and instance.yes and instance.yes <= instance.no:
            self.fields['done'].disabled = True


class TaskForm(ItemForm):
    class Meta:
        model = m.Resolution
        fields = '__all__'


class ProtocolMailForm(forms.Form):
    subject = forms.CharField(
        label=_("Betreff"), required=True,
        widget=forms.TextInput({'style': 'width: 100%;'}))
    body = forms.CharField(
        label=_("E-Mail"), required=True,
        widget=forms.Textarea({'rows': 8, 'style': 'width: 100%;'}))
    from_email = forms.CharField(
        label=_("Von"),
        widget=forms.TextInput(), disabled=True)
    to = JSONListField(
        label=_("An"),
        widget=forms.TextInput(),
        help_text=_("Komma-separierte Liste von E-Mail-Adressen"))
    reply_to = JSONListField(
        label=_("Antwort an"), required=False,
        widget=forms.TextInput(),
        help_text=_("Komma-separierte Liste von E-Mail-Adressen"))

    def __init__(self, *args, **kwargs):
        attachment_name = basename(kwargs.pop('attachment_name'))
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(
                _("Anhang: %s") % (attachment_name,),
                Div(
                    Field('from_email', style="width: 100%"),
                    Field('to', style="width: 100%"),
                    Field('reply_to', style="width: 100%"),
                    css_class="uniFormRow columns-3"),
                'subject',
                'body'
            )
        )
