import logging
import os
import re
from collections import defaultdict
from functools import cached_property
from operator import attrgetter
from pathlib import Path
import mimetypes

import bleach
import yaml
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.template import Context, Engine
from django.utils.safestring import SafeString
from django.utils.timezone import now
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from . import helpers as h

engine = Engine.get_default()
logger = logging.getLogger('buvo')

PROTOCOL_PATH: Path = settings.MEDIA_ROOT / 'converted-protocols'
ATTACHMENT_PATH = 'attachments'


class CrdtBlob(models.Model):
    class Meta:
        verbose_name = _("CRDT Raum")
        verbose_name_plural = _("CRDT Räume")

    blob = models.BinaryField(null=True)
    room_name = models.CharField(max_length=256, primary_key=True)
    timestamp = models.DateTimeField(auto_now=True)


class Protocol(models.Model):
    class Meta:
        verbose_name = _("Protokoll")
        verbose_name_plural = _("Protokolle")

    protocol_md = models.TextField(_("Protokolltext als Markdown"))
    protocol_html = models.TextField(_("Internes Protokoll als HTML"), blank=True)
    protocol_intern_typ = models.FileField(
        _("Internes Protokoll als Typst"), null=True, blank=True,
        default=None, upload_to=PROTOCOL_PATH)
    protocol_intern_odt = models.FileField(
        _("Internes Protokoll als ODT"), null=True, blank=True,
        default=None, upload_to=PROTOCOL_PATH)
    protocol_intern_pdf = models.FileField(
        _("Internes Protokoll als PDF"), null=True, blank=True,
        default=None, upload_to=PROTOCOL_PATH)
    protocol_extern_typ = models.FileField(
        _("Externes Protokoll als Typst"), null=True, blank=True,
        default=None, upload_to=PROTOCOL_PATH)
    protocol_extern_odt = models.FileField(
        _("Externes Protokoll als ODT"), null=True, blank=True,
        default=None, upload_to=PROTOCOL_PATH)
    protocol_extern_pdf = models.FileField(
        _("Externes Protokoll als PDF"), null=True, blank=True,
        default=None, upload_to=PROTOCOL_PATH)
    downloads_are_latest = models.BooleanField(
        _("Downloads sind aktuell"), default=False, editable=False)
    session_type = models.CharField(_("Sitzungsart"), max_length=100, blank=True)
    start_date = models.DateField(_("Start"), blank=True)
    end_date = models.DateField(_("Ende"), blank=True)
    location = models.CharField(_("Sitzungsort"), max_length=100, blank=True)
    attendees = models.JSONField(_("Anwesende"), default=list)
    recorder = models.JSONField(_("Protokoll"), default=list)
    moderation = models.JSONField(_("Sitzungsleitung"), default=list)
    unknown_meta = models.JSONField(_("Weitere Metadaten"), default=dict, blank=True)
    search_dump = models.TextField(blank=True)
    # crdt_blob = models.ForeignKey(CrdtBlob, on_delete=models.SET_NULL, null=True, default=None)

    @property
    def title(self):
        if self.start_date == self.end_date:
            return gettext("%(meeting)s in %(location)s am %(start_date)s") % {
                'meeting': self.session_type,
                'location':self.location,
                'start_date': self.start_date.strftime("%d.%m.%Y")}

        return gettext("%(meeting)s in %(location)s vom %(start_date)s bis %(end_date)s") % {
            'meeting': self.session_type,
            'location': self.location,
            'start_date': self.start_date.strftime("%d.%m."),
            'end_date': self.end_date.strftime("%d.%m.%Y")}

    def _fname(self, field: str) -> str:
        return str(h.verbose_name(self, field))

    @property
    def yaml_frontmatter(self) -> str:
        frontmatter_data = {
            "title": self.title,
            "meta": {
                self._fname('attendees'): self.attendees,
                self._fname('moderation'): self.moderation,
                self._fname('recorder'): self.recorder,
            }
        }
        return yaml.dump(frontmatter_data, allow_unicode=True)

    @property
    def mailstyle_frontmatter(self) -> str:
        attrs = [
            'session_type', 'location', 'start_date', 'end_date',
            'attendees', 'moderation', 'recorder']
        attr_str = ""
        for attr_name in attrs:
            attr_verbose = self._fname(attr_name)
            attr_value = getattr(self, attr_name, "")
            attr_str += h.mailstyle_header_line(attr_verbose, attr_value)
        for attr_name, values in self.unknown_meta.items():
            attr_str += h.mailstyle_header_line(attr_name, values)
        return attr_str

    @cached_property
    def meta_md_split(self):
        return h.extract_header_meta(self.protocol_md)

    @property
    def md_intern(self) -> str:
        txt = self.protocol_md
        txt = h.comment_comments(txt)
        txt = h.remove_intern_markers(txt)
        return txt

    @property
    def md_extern(self) -> str:
        txt = self.protocol_md
        txt = h.comment_comments(txt)
        txt = h.comment_intern(txt)
        return txt

    @property
    def md_editable(self):
        return self.mailstyle_frontmatter + self.protocol_md

    def md_appendices(self, internal: bool, standalone: bool) -> SafeString:
        appendices = defaultdict(list)
        for attachment in self.attachments.all():
            if attachment.internal is internal:
                appendices[attachment.appendix_name].append(attachment)
        ctx = Context({
            'appendices': sorted(
                (key, sorted(attachment_list, key=attrgetter('position')))
                for key, attachment_list in appendices.items()),
            'standalone': standalone})
        return engine.render_to_string('tasks/attachment.md', ctx)

    def md_string(self, body, appendices) -> str:
        ctx = Context({'mdheader': self.yaml_frontmatter, 'mdbody': body, 'appendices': appendices})
        md = engine.render_to_string('tasks/full.md', ctx)
        return bleach.clean(md)

    def make_html(self):
        body = self.md_intern
        appendices = self.md_appendices(internal=True, standalone=False)
        md_string = self.md_string(body, appendices)
        self.protocol_html = h.md_to_html(md_string)

    def make_downloads(self):
        session_date = self.start_date.strftime("%Y-%m-%d")

        def file_name(v, ext):
            n = f"{session_date} {self.session_type} in {self.location} ({v}).{ext}"
            n = n.replace("/", "-")
            return PROTOCOL_PATH / n

        os.makedirs(PROTOCOL_PATH, exist_ok=True)

        # make internal protocols
        intern_odt = file_name(_('intern'), 'odt')
        h.md_to_odt(
            self.md_string(self.md_intern, self.md_appendices(internal=True, standalone=True)),
            str(intern_odt))
        self.protocol_intern_odt.name = str(h.relative_to_media_root(intern_odt))

        intern_typ = file_name(_('intern'), 'typ')
        intern_pdf = file_name(_('intern'), 'pdf')
        h.md_to_typst(
            self.md_string(self.md_intern, self.md_appendices(internal=True, standalone=True)),
            str(intern_typ))
        h.typst_to_pdf(str(intern_typ), str(intern_pdf))
        self.protocol_intern_typ.name = str(h.relative_to_media_root(intern_typ))
        self.protocol_intern_pdf.name = str(h.relative_to_media_root(intern_pdf))

        # make external protocols
        extern_odt = file_name(_('extern'), 'odt')
        h.md_to_odt(
            self.md_string(self.md_extern, self.md_appendices(internal=False, standalone=True)),
            str(extern_odt))
        self.protocol_extern_odt.name = str(h.relative_to_media_root(extern_odt))

        extern_typ = file_name(_('extern'), 'typ')
        extern_pdf = file_name(_('extern'), 'pdf')
        h.md_to_typst(
            self.md_string(self.md_extern, self.md_appendices(internal=False, standalone=True)),
            str(extern_typ))
        h.typst_to_pdf(str(extern_typ), str(extern_pdf))
        self.protocol_extern_typ.name = str(h.relative_to_media_root(extern_typ))
        self.protocol_extern_pdf.name = str(h.relative_to_media_root(extern_pdf))

        self.downloads_are_latest = True
        self.save()

    @property
    def attendee_names(self):
        """ :returns: the list of attendees without the meta information in parentheses """
        return h.extract_names(self.attendees)

    def extract_todos(self):
        Task.objects.filter(protocol=self.pk, done=False).delete()
        existing_texts = {text for text, in self.task_set.all().values_list('text')}
        todos = h.extract_todos(self.protocol_md, h.todo)

        errors = []
        tasks = []
        for text in todos:
            if text not in existing_texts:
                try:
                    tasks.append(Task(
                        text=text, creation_date=self.start_date, protocol=self,
                        due_date=h.extract_date(text, self.start_date),
                        assignees=h.extract_assignees(text, self.attendee_names)))
                except Exception as e:
                    errors.append(e.args[0])

        if errors:
            raise ValidationError(errors)
        Task.objects.bulk_create(tasks)

    def extract_resolutions(self):
        Resolution.objects.filter(protocol=self.pk, done=False).delete()
        existing_texts = {text for text, in self.resolution_set.all().values_list('text')}
        resolution_txts = h.extract_todos(self.protocol_md, h.resolution)

        errors = []
        resolutions = []
        for text in resolution_txts:
            if text not in existing_texts:
                try:
                    # use end date as creation date because resolutions
                    # are settled at the end of a meeting
                    resolutions.append(Resolution(
                        text=text, creation_date=self.end_date, protocol=self,
                        due_date=h.extract_date(text, self.start_date),
                        assignees=h.extract_assignees(text, self.attendee_names),
                        **h.extract_result(text)))
                except Exception as e:
                    errors.append(e.args[0])

        if errors:
            raise ValidationError(errors)
        Resolution.objects.bulk_create(resolutions)

    field_map = {
        (re.compile(r'^(Start|Beginn|Von)$', re.I),                 'start_date'),
        (re.compile(r'^(Ende|Bis)$', re.I),                         'end_date'),
        (re.compile(r'^(Sitzungs?)?(Ort|Stadt)$', re.I),            'location'),
        (re.compile(r'^Anwesen(de?|heit)$', re.I),                  'attendees'),
        (re.compile(r'^Protokoll(führung|ant[*:_]?[Ii]n)?$', re.I), 'recorder'),
        (re.compile(r'^(Art|Sitzung(s(art|typ))?|Typ)$', re.I),     'session_type'),
        (re.compile(r'^(Moderat(ion|tor[*:_]?innen)|Sitzungsleitung)$', re.I), 'moderation'),
    }

    def extract_metadata(self):
        all_meta, remaining_txt = h.extract_header_meta(self.protocol_md)
        known_meta, unknown_meta = h.normalize_known_meta_keys(all_meta, self.field_map)
        meta = h.Chain(known_meta)

        error_dict = dict()

        check_the_date = _(
            "Das %(name)s am Beginn der Protokolldatei konnte nicht verarbeitet werden. "
            "Bitte überprüfe, ob es ein korrektes Format hat (TT.MM.JJJJ).")

        # parse dates
        start_date_str = meta['start_date'][0].obj
        try:
            start_date = start_date_str and h.extract_date(start_date_str)
        except Exception:
            start_date = None
        if start_date_str and not start_date:
            error_dict['start_date'] = ValidationError(check_the_date % {'name': _("Start-Datum")})
            meta.obj['start_date'] = None
        else:
            meta.obj['start_date'] = start_date

        # end_date is optional if the session is only on a single day -> start is enough.
        end_date_str = meta['end_date'][0].obj
        try:
            end_date = end_date_str and h.extract_date(end_date_str)
        except Exception:
            end_date = None
        if end_date_str and not end_date:
            error_dict['end_date'] = ValidationError(check_the_date % {'name': _("End-Datum")})
            meta.obj['end_date'] = None
        else:
            meta.obj['end_date'] = end_date or start_date

        # extract single value fields
        meta.obj['location'] = meta['location'][0].obj
        meta.obj['session_type'] = meta['session_type'][0].obj

        # apply
        self.protocol_md = remaining_txt
        for key, value in meta.obj.items():
            if value:
                setattr(self, key, value)
        self.unknown_meta = unknown_meta

        if error_dict:
            raise ValidationError(error_dict)

    def full_clean(self, *args, **kwargs) -> None:
        errors = {}

        try:
            self.extract_metadata()
        except ValidationError as e:
            errors = e.update_error_dict(errors)

        try:
            super().full_clean(*args, **kwargs)
        except ValidationError as e:
            errors = e.update_error_dict(errors)

        if errors:
            raise ValidationError(errors)

        # nothing more for now
        self.search_dump = self.title
        self.downloads_are_latest = False

    def clean(self) -> None:
        error_dict = dict()

        if not self.start_date:
            error_dict['start_date'] = ValidationError(_(
                "Das Start-Datum wurde weder manuell eingegeben, "
                "noch ist es im Protokoll zu finden."))

        if not self.end_date and not self.start_date:
            error_dict['end_date'] = ValidationError(_(
                "Das End-Datum wurde weder manuell eingegeben, "
                "noch ist es im Protokoll zu finden. "
                "Auch ist kein Start-Datum angegeben."))

        if not self.location:
            error_dict['location'] = ValidationError(_(
                "Der Ort wurde weder manuell eingegeben, "
                "noch ist er im Protokoll zu finden."))

        if not self.session_type:
            error_dict['session_type'] = ValidationError(_(
                "Die Sitzungsart wurde weder manuell eingegeben, "
                "noch ist sie im Protokoll zu finden."))

        if error_dict:
            raise ValidationError(error_dict)

        super().clean()

    def __str__(self):
        return self.title


class Attachment(models.Model):
    class Meta:
        verbose_name = _("Angehänge Datei")
        verbose_name_plural = _("Angehänge Dateien")

    SUPPORTED_MIME_TYPES = {
        'image/jpeg', 'image/png', 'image/gif', 'image/svg',
        # 'application/eps', 'image/eps', 'application/pdf',
        # TODO: support POST-Script attachments
    }

    internal = models.BooleanField(_("Intern"), default=True)
    protocol = models.ForeignKey(
        Protocol, verbose_name=_("Protokoll"), related_name='attachments', on_delete=models.CASCADE)
    position = models.IntegerField(_("Position"), default=1)
    file = models.FileField(_("Datei"), upload_to=ATTACHMENT_PATH)
    appendix_name = models.CharField(_("Names des Anhangs"), max_length=256)
    caption = models.TextField(_("Bildunterschrift"))

    def clean(self) -> None:
        mime, __ = mimetypes.guess_type(self.file.name)
        if mime not in self.SUPPORTED_MIME_TYPES:
            raise ValidationError(
                _("Der Anhang hat den Typen %(mime)s. Die erlaubten Typen lauten: %(allowed)s")
                % {'mime': mime, 'allowed': h.json_array_to_str(self.SUPPORTED_MIME_TYPES)})
        super().clean()


class Item(models.Model):
    class Meta:
        abstract = True

    text = models.TextField(_("Aufgabentext"))
    done = models.BooleanField(_("Erledigt"), default=False)
    creation_date = models.DateField(_("Erstellungsdatum"), null=True, editable=False)
    due_date = models.DateField(_("Fälligkeitsdatum"), null=True, blank=True)
    assignees = models.JSONField(_("Zuständige"), default=list)
    protocol = models.ForeignKey(
        Protocol, verbose_name=_("Protokoll"), blank=True, null=True,
        on_delete=models.SET_NULL, default=None)

    def clean(self) -> None:
        if not self.creation_date:
            # can't use auto now because this could also be set by the protocol class
            self.creation_date = now().date()
        super().clean()

    @property
    def status(self):
        raise NotImplementedError('status property must be implemented on ancestors')

    def __str__(self):
        return f"{self.status}: {self.text}"


class Task(Item):
    class Meta:
        verbose_name = _("Aufgabe")
        verbose_name_plural = _("Aufgaben")

    @property
    def status(self):
        return _("DONE") if self.done else _("TODO")

    def __str__(self):
        return f"{self.status}: {self.text}"


class Resolution(Item):
    class Meta:
        verbose_name = _("Beschluss")
        verbose_name_plural = _("Beschlüsse")

    yes = models.IntegerField(_("Ja"), default=None, null=True, blank=True, help_text=_(
        "Wenn dieses Feld leer ist,<br/>wurde einstimmig angenommen."))
    no = models.IntegerField(_("Nein"), default=0)
    abstention = models.IntegerField(_("Enthaltung"), default=0)

    def clean(self) -> None:
        if self.yes is None and (self.no > 0 or self.abstention > 0):
            raise ValidationError({'yes': _(
                "Ja wurde für einen Beschluss nicht angegeben, "
                "welcher nicht einstimmig war: „%s“"
            ) % (self.text)})
        super().clean()

    @property
    def status(self):
        return _("ABGELEHNT") if (self.yes is not None and self.yes <= self.no) \
            else _("UMGESETZT") if self.done \
            else _("BESCHLOSSEN")
