import base64
import logging
import re
from typing import Set

from channels.db import database_sync_to_async
from channels.generic import websocket
from django.core.signing import BadSignature, Signer

from . import models as m

camel_case = re.compile(r'(?!^)([A-Z]+)')

logger = logging.getLogger("buvo")
signer = Signer()

def camel_to_snake(s):
    return camel_case.sub(r'_\1', s).lower()


class Lib0websocketConsumer(websocket.AsyncJsonWebsocketConsumer):
    allowed_methods: Set[str] = {'ping'}

    async def receive_json(self, json_data, **kwargs):
        # msg_type = camel_to_snake(json_data['type'])
        msg_type = json_data['type']
        # logger.debug(f'received json: {json_data} in {self.__class__.__name__}')
        if msg_type in self.allowed_methods:
            method = getattr(self, msg_type)
            return await method(**json_data)
        raise ValueError(f'The message type "{msg_type}" is not allowed.')

    async def ping(self, **kwargs):
        await self.send_json({'type': 'pong'})


class SignalingConsumer(Lib0websocketConsumer):
    allowed_methods = {'subscribe', 'unsubscribe', 'publish', 'ping'}
    channel_prefix = "signal_"

    @classmethod
    def group_name(cls, topic):
        return cls.channel_prefix + topic

    async def connect(self):
        self.subscribed_topics = set()
        await self.accept()

    async def disconnect(self, close_code):
        await self.unsubscribe(self.subscribed_topics)

    async def subscribe(self, topics=None, **kwargs):
        topics = topics or list()
        for topic in topics:
            await self.channel_layer.group_add(
                self.group_name(topic),
                self.channel_name)
        self.subscribed_topics.update(topics)

    async def unsubscribe(self, topics=None, **kwargs):
        topics = topics or list()
        for topic in topics:
            await self.channel_layer.group_discard(
                self.group_name(topic),
                self.channel_name)
        self.subscribed_topics.difference_update(topics)

    async def publish(self, topic=None, **kwargs):
        if topic:
            await self.channel_layer.group_send(
                self.group_name(topic),
                {'topic': topic, **kwargs, 'type': 'notify_publish'})

    async def notify_publish(self, event: dict):
        await self.send_json({**event, 'type': 'publish'})


class CrdtPersistanceConsumer(Lib0websocketConsumer):
    allowed_methods = {'save_blob', 'ping'}

    async def save_blob(self, room='', password='', blob='', **kwargs):
        try:
          if signer.unsign(password) == room:
                blob = base64.b64decode(blob)
                upsert_blob = database_sync_to_async(m.CrdtBlob.objects.update_or_create)
                blob_obj, _ = await upsert_blob(room_name=room, defaults={'blob': blob})
                logger.debug(f'BLOB SAVED with ID {blob_obj.pk}')
                return
        except BadSignature:
            pass
        logger.warning(f"client tried to fiddle with the password!")
