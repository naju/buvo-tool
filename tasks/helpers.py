import logging
import pathlib
import re
import subprocess
from datetime import date, datetime
from typing import Any, Dict, Iterable, List, Optional, Pattern, Set, Tuple, Type
from django.core.exceptions import ValidationError
from django.forms.utils import ErrorDict, ErrorList

import pypandoc
from django.conf import settings
from django.db.models import Model
from django.template import Context, Template
from django.template.loader_tags import BlockNode, ExtendsNode
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger("buvo")

# region text editors

comment_start = re.compile(r'(?P<format>[_*]{0,2})STARTKOMMENTAR(?P=format)')
comment_end = re.compile(r'(?P<format>[_*]{0,2})ENDEKOMMENTAR(?P=format)')
comment = re.compile(r'[*_]{0,2}STARTKOMMENTAR.*?ENDEKOMMENTAR[*_]{0,2}')
single_line_comment = re.compile(r'([*_]{0,2}KOMMENTAR.*)$', flags=re.MULTILINE)

def comment_comments(txt:str) -> str:
    """
    WARNING: don't use comments inside of intern blocks!
    """
    txt = comment_start.sub("<!--", txt)
    txt = comment_end.sub("-->", txt)
    txt = single_line_comment.sub("<!-- \1 -->", txt)
    return txt

def remove_comments(txt:str) -> str:
    txt = comment.sub("", txt)
    txt = single_line_comment.sub("", txt)
    return txt

intern_start = re.compile(r'[*_]{0,2}STARTINTERN[*_]{0,2}')
intern_end = re.compile(r'[*_]{0,2}ENDEINTERN[*_]{0,2}')
single_line_intern = re.compile(r'^(.*INTERN.*)$', flags=re.MULTILINE)
intern_removable = re.compile(r'[*_]{0,2}(START|ENDE)?INTERN[*_]{0,2} *')

def comment_intern(txt:str) -> str:
    txt = intern_start.sub("<!--", txt)
    txt = intern_end.sub("-->", txt)
    txt = single_line_intern.sub("<!-- \1 -->", txt)
    return txt

def remove_intern_markers(txt:str) -> str:
    return intern_removable.sub("", txt)

# endregion text editors

# region text parsers

list_regex = re.compile(r"\s*,\s*")
# list_regex = re.compile(r"\s*[^\\],\s*")

def split_txt_list(values):
    return [v for v in list_regex.split(values) if v]

header_line = re.compile(r'(?P<format>[_*]{0,2})(?P<key>[-\w ]+):(?P=format)[ \t]*(?P<value>.*)\r?\n')
newlines = re.compile(r'(\r?\n)*')

MetaDict = Dict[str, List[str]]

def extract_header_meta(txt: str) -> Tuple[MetaDict, str]:
    """
    :returns: The dict containing the metadata and the remaining text without the metadata. \
        The values of the headers are always returned as a list.

    Headers must be at the beginning of the file, starting at the very first character. They
    adhere to the following format::

        Key 1: Value 1, Value 2, Value 3, ...

        Key 2: Value 1, Value 2, Value 3, ...

        ...

    One header always ends with two newlines. It may be surrounded by the Markdown formatting
    symbols for italic or bold. It can contain:

    * alphanumeric unicode word characters,
    * the symbol ``-`` (hyphen) and space characters.
    """
    meta = dict()
    while (match := header_line.match(txt)):
        key = match.group('key')
        values = match.group('value')
        values = split_txt_list(values)
        # values = [value.replace('\\,', ',') for value in values]
        meta[key] = values
        txt = txt[match.end():]
        nl_end = newlines.match(txt)
        nl_end = nl_end.end() if nl_end else 0
        txt = txt[nl_end:]
    return meta, txt

FieldMap = Set[Tuple[re.Pattern, str]]

def normalize_known_meta_keys(meta: MetaDict, field_map: FieldMap) -> Tuple[MetaDict, MetaDict]:
    """
    Normalizes known meta keys to field names.

    :param meta: the metadata, usually extracted from the head of a document.

    :param field_dict: a list of tuples conaining the regex to execute \
        on each key and the normalized field name.

    :returns: a tuple of dicts where the first dict contains only the values \
        that matched the given ``field_map`` and the second dict contains all \
        unknown keys.
    """
    matched_meta: MetaDict = dict()
    unmatched_meta: MetaDict = dict()
    matched_patterns: FieldMap = set()

    for key, values in meta.items():
        for pattern, field_name in field_map.difference(matched_patterns):
            if pattern.match(key):
                matched_meta[field_name] = values
                matched_patterns.add((pattern, field_name,))
                break
        else:
            unmatched_meta[key] = values

    return matched_meta, unmatched_meta

date_int = re.compile(r'((?P<year>\d{4})-)?(?P<month>\d{1,2})-(?P<day>\d{1,2})')
date_ger = re.compile(r'(?P<day>\d{1,2})\.(?P<month>\d{1,2})\.(?P<year>\d{2,4})?')

def extract_date(task: str, current: Optional[date] = None) -> Optional[date]:
    """
    Extracts the first date that can be found in a task. If the date does not
    contain a year, it is assumed that the date is the one that follows the
    date given in `current`.

    :param task: the single line task string to extract from.

    :param current: the current / creation date of the task. It is set to \
        the date of execution if it is not given as an argument.

    :returns: the first date to be found in the given string, if there is any. \
        `None` otherwise.

    :raises ValueError: if a date could not be parsed
    """
    current = current or now().date()
    date_found = date_int.search(task) or date_ger.search(task)

    if date_found:
        kw = {k: int(v) for k, v in date_found.groupdict().items() if v}
        year_added = False
        if "year" not in kw:
            kw["year"] = current.year
            year_added = True
        elif kw["year"] < 100:
            kw["year"] += current.year // 100 * 100

        try:
            date_found = date(**kw)
        except ValueError as e:
            raise ValueError(f"Task “{task}”: {e.args[0]}")

        if year_added and date_found < current:
            kw["year"] = current.year + 1
            date_found = date(**kw)

    return date_found

extra_information = re.compile(r' *\((?P<extra>.*?)\) *')

def extract_names(values: List[str]) -> List[str]:
    """
    extracts a name from a value that contains additional data about that name::

        "Mr X (meta info)" -> "Mr X"
    """
    return [extra_information.sub('', value) for value in values]

who = re.compile(r'wer:(?P<wer>.*?)\s')

def extract_assignees(task: str, names: List[str]) -> List[str]:
    """
    :returns: a list of assignee names either from the given \
        list or that have been prefixed with ``wer:``

    :param task: a single line string to extract the information from.

    :param names: a list of names. For the lookup, these are split by \
        space characters, so given names as well as surnames can be found.
    """
    name_dict = dict()
    for name in names:
        for part in name.split(" "):
            if part:
                name_dict[re.escape(part)] = name
    regex = re.compile("|".join(name_dict.keys()))
    assignees = regex.findall(task)
    while (match := who.search(task)):
        assignees += match.group('wer').split(',')
        task = task[match.end()]
    return list(set(assignees)) # maybe not the most efficient way for deduplication…

todo_pre = r'^(([ \t]*[*+-] +)?(?P<format>[_*]{0,2})'
todo_post = r':?(?P=format):? *)(?P<todo_text>.*)$'
todo = re.compile(todo_pre + r'ACT' + todo_post, flags=re.MULTILINE)
resolution = re.compile(todo_pre + r'BESCHLUSS' + todo_post, flags=re.MULTILINE)

def extract_todos(txt: str, regex: Pattern) -> List[str]:
    """
    Extract todos from the given text.

    :param txt: the text to extract from

    :param regex: the pattern used to extract. Should be one of \
        :py:member:`.todo` or :py:member:`.resolution`.
    """
    todos = []
    while (match := regex.search(txt)):
        todos.append(match.group('todo_text').strip())
        txt = txt[match.end():]
    return todos

result = re.compile(r'(((?P<yes>\d+)[JjYy]-?)'
                    r'|((?P<no>\d+)[Nn]-?)'
                    r'|((?P<abstention>\d+)[Ee]-?)'
                    r'){2,3}')

def extract_result(resolution: str):
    resultdict = result.search(resolution).groupdict()
    return {
        'yes': int(resultdict['yes']) if resultdict['yes'] else None,
        'no': int(resultdict['no']) if resultdict['no'] else 0,
        'abstention': int(resultdict['abstention']) if resultdict['abstention'] else 0,
    }

# endregion text parsers

# region document converters

def md_to_html(md: str, toc_depth=2) -> str:
    pandoc_args = [
        '--toc', f'--toc-depth={toc_depth}',
        '-M', f'toc-title={_("Inhaltsverzeichnis")}',
        '-V', 'show-meta',
        '--strip-comments',
        '--template=protocol', f'--data-dir={settings.PANDOC_DIR}']
    return pypandoc.convert_text(md, 'html', format='markdown', extra_args=pandoc_args)

def md_to_typst(md: str, filename: str, toc_depth=2) -> str:
    pandoc_args = [
        '--toc', f'--toc-depth={toc_depth}',
        '-V', 'show-meta',
        '--strip-comments',
        '--template=protocol', f'--data-dir={settings.PANDOC_DIR}']
    pypandoc.convert_text(
        md, 'typst', format='markdown+raw_tex',
        extra_args=pandoc_args, outputfile=filename
    )

def md_to_odt(md: str, filename: str, toc_depth=2):
    pandoc_args = [
        '--toc', f'--toc-depth={toc_depth}',
        '-M', f'toc-title={_("Inhaltsverzeichnis")}', '-M', f"toc-depth={toc_depth}",
        '-V', 'show-meta',
        '--strip-comments',
        f'--data-dir={settings.PANDOC_DIR}']
    logger.info(f'generating {filename} with pandoc')
    pypandoc.convert_text(md, 'odt', format='markdown', extra_args=pandoc_args, outputfile=filename)

def typst_to_pdf(infile, outfile):
    logger.info(f'generating pdf from {infile} with typst as {outfile}')
    cmd = [
        settings.TYPST_BIN, "compile", "--root", str(settings.TYPST_ROOT),
        "--font-path", str(settings.BASE_DIR.absolute() / "pandoc" / "fonts"),
        infile, outfile
    ]
    logger.info(f'executing: {cmd}')
    completed = subprocess.run(cmd, capture_output=True)
    log_result(completed, "Typst")

def log_result(
    result: subprocess.CompletedProcess | subprocess.TimeoutExpired,
    program: str = "Typst"
):
    if result.stdout:
        logger.info(f"{program}: " + result.stdout.decode('utf-8'))
    if result.stderr:
        logger.error(f"{program}: " + result.stderr.decode('utf-8'))

# endregion document converters

# region django helpers

def relative_to_media_root(path: pathlib.Path):
    return path.relative_to(settings.MEDIA_ROOT)

def verbose_name(cls: Type[Model], fieldname: str):
    return cls._meta.get_field(fieldname).verbose_name

def render_template_node(template: Template, context: Context, block_name: str):
    """ Can be used to render one block only. """
    for node in template:
        if isinstance(node, BlockNode) and node.name == block_name:
            context.template = template # workaround
            return node.render(context)
        elif isinstance(node, ExtendsNode):
            return render_template_node(node.nodelist, context, block_name)
    raise Exception("Node '%s' could not be found in template." % block_name)

def error_to_error_dict(error: Exception):
    """
    partly taken form py::method``django.forms.BaseForm.add_error``
    """
    if not isinstance(error, ValidationError):
        # Normalize to ValidationError and let its constructor
        # do the hard work of making sense of the input.
        error = ValidationError(error)

    if hasattr(error, 'error_dict'):
        error: dict = ErrorDict(error.error_dict)
        for key, value in error.items():
            if isinstance(value, list):
                error[key] = ErrorList(value)
                for idx, list_value in enumerate(error[key]):
                    error[key][idx] = error_to_error_dict(list_value)
    elif not hasattr(error, 'message'):
        # if message is present, this is a leaf ValidationError
        error = ErrorList(error_to_error_dict(l) for l in error.error_list)
    return error


class Chain:
    """
    A class for optional chaining in Python.

    Contains a tree of ``dict``s, ``list``s and ``object``s, that can be queried via ``__getitem__``
    (``[...]``). The object contained in the class can be retrieved via ``.obj``. If any of the
    items or attributes in the getter chain contains ``None``, ``.obj`` will be None, too.
    """
    def __init__(self, obj: Any) -> None:
        self.obj = obj

    def get(self, key, default=None):
        if isinstance(self.obj, dict):
            return Chain(self.obj.get(key, None))
        if isinstance(self.obj, list):
            if key < len(self.obj) and key >= 0:
                return Chain(self.obj[key])
        if isinstance(key, str):
            return Chain(getattr(self.obj, key, None))
        return Chain(default)

    __getitem__ = get

# endregion django helpers

# region formatters

def json_array_to_str(json: Optional[Iterable[str]]) -> str:
    return ", ".join(json) if json else ""

def mailstyle_header_line(key, value, key_format="**") -> str:
    """
    formats a value to its header format.

    :param key: Header name

    :param value: Header value

    :param key_format: markdown formatting symbol for header name
    """
    if isinstance(value, datetime):
        value = value.strftime("%d.%m.%Y")
    if isinstance(value, list):
        value = json_array_to_str(value)
    return f"{key_format}{key}:{key_format} {value}\n\n"

# endregion formatters
