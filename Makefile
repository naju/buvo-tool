SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

BT_VERSION := $(shell grep -hE '^__version__ = "' buvo/__init__.py | grep -ohE '(\d\.?)+')
BT_FLUID_VERSION := latest

.PHONY: latest upload-latest upload no-cache run

PLATFORM := linux/x86-64

latest:
	docker build \
		-t buvo-tool\:$(BT_FLUID_VERSION) -t buvo-tool\:$(BT_VERSION) \
		-t registry.gitlab.com/naju/buvo-tool\:$(BT_FLUID_VERSION) \
		-t registry.gitlab.com/naju/buvo-tool\:$(BT_VERSION) \
		--platform $(PLATFORM) $(BUILDFLAGS) .

no-cache:
	docker build \
		-t buvo-tool\:$(BT_FLUID_VERSION) -t buvo-tool\:$(BT_VERSION) \
		-t registry.gitlab.com/naju/buvo-tool\:$(BT_FLUID_VERSION) \
		-t registry.gitlab.com/naju/buvo-tool\:$(BT_VERSION) \
		--no-cache --platform linux/x86-64 $(BUILDFLAGS) .

upload-latest: latest
	docker push registry.gitlab.com/naju/buvo-tool\:$(BT_VERSION)
	docker push registry.gitlab.com/naju/buvo-tool\:$(BT_FLUID_VERSION)

upload: upload-latest

run: latest
	docker compose up
