#!/bin/sh
# TODO: run this periodically via:
# docker-compose exec --privileged -u root buvo-tool sh upgrade.sh
# && docker-compose restart buvo-tool

apt update
apt upgrade -y
pip install -U -r requirements.txt
