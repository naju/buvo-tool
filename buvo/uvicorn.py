from uvicorn.workers import UvicornWorker as BaseUvicornWorker
from os import environ


class UvicornWorker(BaseUvicornWorker):
    CONFIG_KWARGS = {
        "loop": "uvloop",
        "http": "httptools",
        "lifespan": "off",
        "root_path": environ.get("SCRIPT_NAME", "")
    }
