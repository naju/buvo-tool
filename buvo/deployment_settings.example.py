from .settings import *

DEBUG = True
SECRET_KEY = "****************************************************"
LIBREOFFICE_BIN = "soffice"

FORCE_SCRIPT_NAME = "/buvo-tool"
STATIC_URL = "/buvo-tool/static/"
MEDIA_URL = "/buvo-tool/media/"
ALLOWED_HOSTS = ["localhost", "some.server.name"]
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
USE_X_FORWARDED_HOST = True

LOGGING['loggers'].update({
    'buvo': {
        'handlers': ['console'],
        'level': 'INFO',
    }
})

DEFAULT_FROM_EMAIL = "NAJU Bundesvorstand <buvo@some.server.name>"
LV_MAIL = ["Landesverbände <lv@some.server.name>"]
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = 'some.server.name'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = '****************************************************'
# EMAIL_HOST_PASSWORD = '****************************************************'
# EMAIL_USE_TLS = True

STUN_SERVER = "stun:some.server.name:3478"
TURN_SERVER = "turn:some.server.name:3478"
TURN_USER = "****************************************************"
TURN_PASSWORD = "****************************************************"
