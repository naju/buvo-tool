from .settings import *
import uuid

ALLOWED_HOSTS.extend(['oslo.fritz.box', '127.0.0.1'])
SECRET_KEY = 'django-insecure-9d9757$awwi9%1ysuv3fir5@##g4ex0txx-$ar83jo#3ofkzjy'
DEBUG = True

LV_MAIL = ['Landesverbände <lv@naju.de>']
DEFAULT_FROM_MAIL = 'buvo@naju.de'
UUID_NAMESPACE = uuid.UUID("183f3dd8-a93f-427b-8e2d-1b9fc5f6eb6e")

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
LOGGING['loggers'].update({
    'buvo': {
        'handlers': ['console'],
        'level': 'DEBUG',
    }
})
STATICFILES_DIRS = [
    BASE_DIR / "editor/dist/"
]
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST
# EMAIL_PORT
# EMAIL_HOST_USER
# EMAIL_HOST_PASSWORD
# EMAIL_USE_TLS
# EMAIL_TIMEOUT

# FORCE_SCRIPT_NAME = "/buvo-tool"
# STATIC_URL = "/buvo-tool/static/"
# MEDIA_URL = "/buvo-tool/media/"
