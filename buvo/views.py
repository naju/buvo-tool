from django.http.response import (HttpResponse, HttpResponseForbidden,
                                  HttpResponseRedirect)
from django.urls import reverse


def admin_redirect_view(request):
    return HttpResponseRedirect(reverse('admin:index'))

def authenticated_view(request):
    if not request.user:
        return HttpResponse('', status=401)
    elif not request.user.is_staff:
        return HttpResponseForbidden('')
    return HttpResponse('', status=200)
