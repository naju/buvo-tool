# Build the editor
FROM node:lts AS editor_builder

WORKDIR /srv

# RUN apt update \
#     && apt upgrade -y \
#     && apt clean \
#     && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN npm i -g pnpm

RUN groupadd -r builder \
    && useradd --no-log-init -r -m -g builder builder \
    && chown builder:builder /srv

COPY --chown=builder:builder \
    ./editor/package.json ./editor/pnpm-lock.yaml \
    ./

USER builder

RUN pnpm install
# RUN pnpx update-browserslist-db@latest

COPY ./editor/src/ ./src/

RUN pnpm build

# ------------------------------------------------------------------------------------------------ #
# Support MariaDB, Postgres, Redis; Download typst

FROM ubuntu:24.04 AS wheel_builder

WORKDIR /opt

RUN apt update \
    && apt upgrade -y \
    && apt install -y --no-install-recommends \
        gcc pkg-config libc-dev libmariadb-dev libpq-dev wget xz-utils \
        python3 python3-pip python3-dev python3-setuptools python3-wheel \
    && apt autoremove -y && apt autoclean && apt clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN groupadd -r builder \
    && useradd --no-log-init -r -m -g builder builder \
    && chown builder:builder /opt

USER builder

COPY --chown=builder:builder ./wheels.requirements.txt .

# make wheels
RUN mkdir -p wheels
RUN python3 -m pip wheel -r wheels.requirements.txt -w ./wheels/

# download packages
RUN mkdir -p pkg typst

ARG TYPST_RELEASE=0.11.1
ARG TYPST_PLATFORM=x86_64
ARG TYPST_DL=v${TYPST_RELEASE}/typst-${TYPST_PLATFORM}-unknown-linux-musl.tar.xz

RUN wget -O pkg/typst.tar.xz https://github.com/typst/typst/releases/download/${TYPST_DL}
RUN tar --strip-components=1 -xvf pkg/typst.tar.xz -C ./typst/

# ------------------------------------------------------------------------------------------------ #
# Main image build stage

FROM ubuntu:24.04 AS buvotool
# uses python 3.12

WORKDIR /srv

# install dependencies
RUN apt update \
    && apt upgrade -y \
    && apt install -y --no-install-recommends \
        gosu libmariadb3 libpq5 pandoc \
        python3 python3-pip python3-setuptools python3-wheel \
    && apt autoremove -y && apt autoclean && apt clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY --from=wheel_builder /opt/typst/typst /usr/local/bin/

COPY --from=wheel_builder /opt/wheels /opt/wheels
COPY ./wheels.requirements.txt .
RUN pip install --break-system-packages --no-cache-dir \
    -r wheels.requirements.txt --find-links /opt/wheels

COPY ./requirements.txt .
RUN pip install --break-system-packages --no-cache-dir -r requirements.txt

COPY ./backends.requirements.txt .
RUN pip install --break-system-packages --no-cache-dir \
    -r backends.requirements.txt --find-links /opt/wheels

# make an unpriviledged user and allow the user to access the data folder
RUN groupadd -r buvotool \
    && useradd --no-log-init -r -m -g buvotool buvotool \
    && mkdir data && chown -R buvotool:buvotool data

USER buvotool

COPY --from=editor_builder /srv/dist/tasks /srv/editor/dist/tasks

COPY . .

ENV DJANGO_SETTINGS_MODULE=buvo.develop_settings

USER root

ENV DJANGO_SETTINGS_MODULE=buvo.deployment_settings

ENTRYPOINT ["./docker_entrypoint.sh"]
CMD ["buvo.asgi", "--bind=0.0.0.0:8000"]

EXPOSE 8000
