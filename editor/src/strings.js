const metaTpl = {
	start: "**Sitzung:** …",
	end: `

**Ort:** …

**Anwesenheit:** …, …

**Sitzungsleitung:** …

**Protokoll:** …

**Start:** TT.MM.JJJJ

**Ende:** TT.MM.JJJJ

`,
}

export { metaTpl }
