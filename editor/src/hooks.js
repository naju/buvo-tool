import { useState, useEffect } from "preact/hooks"

/**
 * Translates the editor state to a hook.
 * @param {import("codemirror").Editor} editor
 * @param {ReturnType<import("./actions").default>} actions
 * @returns the editor state as a hookable state object
 */
export function useEditorState(editor, actions) {
	const [state, setState] = useState({
		isBold: false,
		isItalic: false,
		isMono: false,
		isStrikethrough: false,
		isLink: false,
		isTable: false,
		paragraphStyle: "p",
	})

	useEffect(() => {
		function updateEditorState() {
			let cursor = editor.getCursor()
			let pStyle
			try {
				pStyle = actions.paragraph.getParagraphStyleAt(cursor)
			} catch (err) {
				pStyle = state.paragraphStyle
			}
			let lineState = editor.getStateAfter(cursor.line)
			let tokenState = editor.getTokenAt(cursor).state

			setState({
				isBold: !!tokenState.strong,
				isItalic: !!tokenState.em,
				isMono: !!tokenState.code,
				isStrikethrough: tokenState.strikethrough,
				isLink: tokenState.linkHref || tokenState.linkText || tokenState.linkTitle,
				isTable: !!lineState.hmdTable,
				paragraphStyle: pStyle,
			})
		}

		editor.on("cursorActivity", updateEditorState)
		editor.on("changes", updateEditorState)
		return () => {
			editor.off("cursorActivity", updateEditorState)
			editor.off("changes", updateEditorState)
		}
	}, [editor, actions])

	return [state]
}

function getAwarenessInfo(awareness) {
	return Array.from(awareness.getStates().values()).sort(({ name: a }, { name: b }) =>
		a < b ? -1 : a > b ? 1 : 0,
	)
}

/**
 * Wraps the awareness protocol into a hookable state
 * @param {import("y-protocols/awareness").Awareness} awareness
 * @returns awareness info as a state
 */
export function useAwareness(awareness) {
	let [awarenessState, setAwareness] = useState(getAwarenessInfo(awareness))
	useEffect(() => {
		const updateAwareness = () => setAwareness(getAwarenessInfo(awareness))
		awareness.on("change", updateAwareness)
		return () => awareness.off("change", updateAwareness)
	}, [awareness])
	return [awarenessState]
}

/**
 *
 * @param {import("yjs").UndoManager} yUndoManager undoManager to wrap
 */
export function useUndoManager(yUndoManager) {
	const [undoState, setUndoState] = useState({
		canUndo: false,
		canRedo: false,
	})

	useEffect(() => {
		function updateUndoState() {
			setUndoState({
				canUndo: yUndoManager.canUndo(),
				canRedo: yUndoManager.canRedo(),
			})
		}

		yUndoManager.on("stack-item-added", updateUndoState)
		yUndoManager.on("stack-item-popped", updateUndoState)
		return () => {
			yUndoManager.off("stack-item-added", updateUndoState)
			yUndoManager.off("stack-item-popped", updateUndoState)
		}
	}, [yUndoManager])

	return [undoState]
}
