import * as Y from "yjs"
import { CodemirrorBinding } from "y-codemirror"
import { WebrtcProvider } from "y-webrtc"
import { IndexeddbPersistence } from "y-indexeddb"
import * as ws from "lib0/websocket"
import { toBase64, fromBase64 } from "lib0/buffer"
import debounce from "lodash/debounce"

import { fromTextArea as CMFromTextArea } from "hypermd"
import "codemirror/addon/dialog/dialog"
import "codemirror/addon/dialog/dialog.css"
import "codemirror/addon/search/search"
import "codemirror/addon/edit/trailingspace"
import "codemirror/addon/selection/active-line"

import { render, h } from "preact"

import { download, getCookie } from "./helpers"

import Sidebar from "./components/Sidebar"
import { makeControls } from "./components/Controls"

import "./style.css"

if (process.env.NODE_ENV == "development") {
	require("preact/debug")
}

// #region YJS SETUP
/********************** SETUP YJS **********************/

const settingsElement = document.getElementById("protocol-jssettings")
const settings = JSON.parse(settingsElement.textContent)

const ydoc = new Y.Doc()
if (settings.persistedDoc) {
	Y.applyUpdate(ydoc, fromBase64(settings.persistedDoc))
	console.log("synced doc from settings")
}

const webrtc = new WebrtcProvider(settings.room, ydoc, {
	signaling: [settings.signaling],
	password: settings.password,
	peerOpts: {
		config: {
			iceServers: [
				{
					urls: settings.turnServer,
					username: settings.turnUser,
					credential: settings.turnPassword,
				},
				{ urls: settings.stunServer },
			],
		},
	},
})

let localPersistance = new IndexeddbPersistence(settings.room, ydoc)

const yText = ydoc.getText("codemirror")
const yUndoManager = new Y.UndoManager(yText)

// #endregion YJS SETUP

// #region CM SETUP
/********************** SETUP CODEMIRROR **********************/

const editorTextArea = document.getElementById("protocol-editor")
const editor = CMFromTextArea(editorTextArea, {
	mode: "hypermd",
	lineNumbers: true,
	table: true,
	styleActiveLine: { nonEmpty: false },
})
editor.setSize("100%", "100%")

const randomEl = (arr) => arr[Math.ceil(Math.random() * (arr.length - 1))]

const binding = new CodemirrorBinding(yText, editor, webrtc.awareness, { yUndoManager })
editor.off("blur", binding._blurListeer) // don't remove cursor on window blur
binding.awareness.setLocalStateField("user", {
	name: settings.user,
	color: randomEl([
		"#0d83b5",
		"#675997",
		"#8f5a96",
		"#be3c79",
		"#d7232f",
		"#e9833f",
		"#f3ec68",
		"#277c43",
		"#679843",
		"#04afa6",
		"#33b9d7",
	]),
})

// #region text fixing

// function fixPastedContent(cm, change) {
// 	if (change.origin == 'paste') {
// 		const newText = change.text.map(fixCommonMistakes);
//     change.update(null, null, newText);
// 	}
// }
// editor.on('beforeChange', fixPastedContent)
// document.getElementById('controls__repair')
// 	.addEventListener('click', e => {
// 		editor.operation(() => {
// 			editor.eachLine((handle) => {
// 				let line = handle.lineNo()
// 				editor.replaceRange(
// 					fixCommonMistakes(handle.text),
// 					{line, ch: 0}, {line}
// 				)
// 			})
// 		})
// 		editor.focus()
// 	})

// #endregion text fixing

// #endregion CM SETUP

// #region PERSISTANCE
/********************** PERSIST ON SERVER **********************/

let wsPersistance = null

// send the blob to the server
const aware = webrtc.awareness
let thisClientRules = false

function setClientRules() {
	let largest = 0
	for (let clientID of aware.getStates().keys()) {
		largest = Math.max(largest, clientID)
	}
	thisClientRules = largest == aware.clientID
}

function yDocBase64() {
	let doc = Y.encodeStateAsUpdate(ydoc)
	return toBase64(doc)
}

const sendUpdateToServer = debounce(
	async () => {
		wsPersistance.send({
			type: "save_blob",
			room: settings.room,
			password: settings.password, // room name signed by server
			blob: yDocBase64(),
		})
	},
	5000,
	{ leading: false, trailing: true, maxWait: 20000 },
)

const consentOnSender = debounce(
	(change) => {
		if (!change?.added && !change?.removed) return
		setClientRules()
		if (!thisClientRules && wsPersistance?.connected) {
			wsPersistance.disconnect()
			console.debug("another client will update the blob on the server now.")
		} else if (thisClientRules && !wsPersistance?.connected) {
			if (!wsPersistance) {
				wsPersistance = new ws.WebsocketClient(settings.persistance)
				wsPersistance.on("connect", (e) => {
					console.debug("connected. client rules:", thisClientRules)
					ydoc.on("afterTransaction", sendUpdateToServer)
				})
				wsPersistance.on("disconnect", (e) => {
					console.debug("disconnected. client rules:", thisClientRules)
					ydoc.off("afterTransaction", sendUpdateToServer)
				})
			} else {
				wsPersistance.connect()
			}
			console.debug("this client is now responsible for sending updates to the server.")
		}
	},
	1000,
	{ leading: false, trailing: true, maxWait: 2000 },
)

const setupNewYDoc = debounce(
	() => {
		setClientRules()
		if (thisClientRules && settings.persistedDoc === null && yText.length === 0) {
			yText.insert(0, editorTextArea.innerText)
		}
		aware.off("change", setupNewYDoc)
	},
	1000,
	{ leading: false, trailing: true, maxWait: 2000 },
)

aware.on("change", consentOnSender)
aware.on("change", setupNewYDoc)

localPersistance.on("synced", () => {
	console.log("synced from indexdb")
	setupNewYDoc()
	consentOnSender()
})

// #endregion PERSISTANCE

// #region SAVING
/********************** SAVING **********************/

async function saveText() {
	let json_body = {
		protocolMd: editor.getValue(),
		blob: yDocBase64(),
	}

	// no url means POST to window.location.href
	let res = await fetch("", {
		headers: {
			"Content-Type": "application/json",
			[settings.csrfHeaderName]: getCookie("csrftoken"),
		},
		cache: "no-cache",
		method: "post",
		body: JSON.stringify(json_body),
		credentials: "same-origin",
	})

	if (res.status != 200) {
		let err = new Error("saving failed or server gave unexpected answer")
		err.json = {
			errorsHtml: `
				<ul class="errorlist"><li>
					Der Server hat eine unerwartete Antwort gegeben.
					Das Speichern hat nciht funktioniert.
				</li></ul>`,
		}
		err.response = res
		throw err
	}

	let json = await res.json()
	if (json.type == "error") {
		let err = new Error("the form was invalid")
		err.json = json
		err.respose = res
		throw err
	}
	return json
}

const errorListContainer = document.getElementById("sidebar__errors")

/**
 * render the error list retrieved from the server
 * @param {Object=} json the json retrieved from the server
 */
function updateErrorList(json) {
	const errorListElement = errorListContainer.querySelector(".errorlist")
	errorListElement && errorListElement.remove()
	switch (json.type) {
		case "success":
			errorListContainer.classList.add("sidebar__errors--noerrors")
			break

		case "error":
			let node = document.createElement("template")
			node.innerHTML = json.errorsHtml
			let content = node.content.cloneNode(true)
			errorListContainer.append(content)
			errorListContainer.classList.remove("sidebar__errors--noerrors")
			break
	}
}

// navigate if another client saved successfully and it was a new document
async function navigate(url) {
	await localPersistance.clearData()
	window.location.href = url
}

let nav
aware.on("change", (change) => {
	if (
		change.updated?.length &&
		change.updated[0] != aware.clientID &&
		(nav = aware.states.get(change.updated[0]).creationSuccessURL)
	) {
		navigate(nav)
	}
})

// navigate the current client
function navigateAfterSaving(json, exit = false) {
	let navigateTo = exit ? json.exitDestinationURL : json.creationSuccessURL
	if (json.creationSuccessURL) {
		aware.on("change", () => navigate(navigateTo))
		aware.setLocalStateField("creationSuccessURL", json.creationSuccessURL)
	} else if (exit) {
		navigate(navigateTo)
	}
}

document.getElementById("sidebar__save").addEventListener("click", async (e) => {
	// disable message processing during save
	ydoc.off("afterTransaction", sendUpdateToServer)
	editor.focus()
	let responseJson
	try {
		responseJson = await saveText()
		await navigateAfterSaving(responseJson)
	} catch (err) {
		responseJson = err.json
	}
	updateErrorList(responseJson)
	ydoc.on("afterTransaction", sendUpdateToServer)
})

document.getElementById("sidebar__save-exit").addEventListener("click", async (e) => {
	// disable message processing before exiting
	// so the server does not store a new blob
	ydoc.off("afterTransaction", sendUpdateToServer)
	try {
		let responseJson = await saveText()
		if (wsPersistance?.connected) {
			wsPersistance.disconnect()
		}
		navigateAfterSaving(responseJson, true)
	} catch (err) {
		// re-enable message processing in case of an error
		ydoc.on("afterTransaction", sendUpdateToServer)
		updateErrorList(err.json)
	}
})

document.getElementById("sidebar__download").addEventListener("click", (e) => {
	download("Protokoll.md", editor.getValue())
})

// #endregion SAVING

// #region SIDEBAR
/********************** SIDEBAR and CONTROLS **********************/

render(h(Sidebar, { awareness: webrtc.awareness }), document.getElementById("sidebar__users"))

const Controls = makeControls(editor, yUndoManager)

render(h(Controls), document.getElementById("protoed__controls"))

/********************** HELP WINDOW **********************/

const helpWindow = document.getElementById("protoed__help")
const helpBackdrop = document.getElementById("help__backdrop")
const helpBtn = document.getElementById("sidebar__help-button")
let windowHidden = true
const toggleHelp = () => {
	if (windowHidden) {
		helpWindow.classList.remove("hidden")
		helpBackdrop.classList.remove("hidden")
		helpWindow.focus()
		windowHidden = false
	} else {
		helpWindow.classList.add("hidden")
		helpBackdrop.classList.add("hidden")
		editor.focus()
		windowHidden = true
	}
}
// toggleHelp()
/* window.addEventListener('keyup', e => {
	if (e.key == "h" && e.ctrlKey) {
		toggleHelp()
	}
}) */
helpBtn.addEventListener("click", toggleHelp)
helpBackdrop.addEventListener("click", toggleHelp)

// #endregion SIDEBAR

if (process.env.NODE_ENV == "development") {
	window.ydoc = ydoc
	window.cm = editor
	window.yundo = yUndoManager
	window.ywebrtc = webrtc
}
