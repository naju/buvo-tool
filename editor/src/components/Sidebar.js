import { Fragment } from 'preact';
import { html } from 'htm/preact'
import { useAwareness } from '../hooks'

function Sidebar({awareness}) {
	const [awarenessInfo] = useAwareness(awareness)

	return html`<${Fragment}>
		<h2 class="sidebar__userlist-heading">Mitarbeitende</h2>
		<ul class="sidebar__userlist">${awarenessInfo.map(({user}) => html`
            <li class="sidebar__user" style="background: ${user.color};">${user.name}</li>
        `)}</ul>
	<//>`
}

export default Sidebar
