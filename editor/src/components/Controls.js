import { Fragment } from "preact"
import { html } from "htm/preact"
import createEditorActions from "../actions"
import { useEditorState, useUndoManager } from "../hooks"
import { assignAndReturn } from "../helpers"

import * as s from "../strings"

/**
 * @param {import("codemirror").Editor} editor codemirror instance
 * @param {import("yjs").UndoManager} yUndoManager yjs history
 * @returns a component which renders the editor control bar
 */
export function makeControls(editor, yUndoManager) {
    const actions = createEditorActions(editor)
    if (process.env.NODE_ENV == "development") {
        window.actions = actions
    }

    function mkExecAndFocus(fn) {
        return (...args) => {
            fn(...args)
            editor.focus()
        }
    }

    function focusAfterEach(actionsPart) {
        return Object.entries(actionsPart).reduce(
            (o, [key, fn]) => assignAndReturn(o, key, mkExecAndFocus(fn)),
            {},
        )
    }

    const tableActions = focusAfterEach(actions.table)
    const inlineActions = focusAfterEach(actions.inline)

    const undo = mkExecAndFocus(() => yUndoManager.undo())
    const redo = mkExecAndFocus(() => yUndoManager.redo())

    const tabInc = mkExecAndFocus(() => editor.execCommand("hmdTab"))
    const tabDec = mkExecAndFocus(() => editor.execCommand("hmdShiftTab"))

    const internal = mkExecAndFocus(() =>
        actions.insert.insertAroundCurrentlySelectedLines(
            "\n*STARTINTERN*\n\n",
            "\n\n*ENDEINTERN*\n",
        ),
    )

    const comment = mkExecAndFocus(() =>
        actions.insert.insertAroundCurrentlySelectedLines(
            "\n*STARTKOMMENTAR*\n\n",
            "\n\n*ENDEKOMMENTAR*\n",
        ),
    )

    const act = mkExecAndFocus(() =>
        actions.insert.addNewParagraphWithSameStyle("*ACT:* wer: ", " bis:"),
    )

    const resolution = mkExecAndFocus(() =>
        actions.insert.addNewParagraphWithSameStyle("*BESCHLUSS:* ", " ergebnis:xJ-xN-xE"),
    )

    const meta = () => {
        editor.setCursor(0, 0)
        actions.insert.insertAroundCurrentSelections(s.metaTpl.start, s.metaTpl.end)
        editor.setCursor(0, s.metaTpl.start.length)
        editor.focus()
    }

    const repair = mkExecAndFocus(actions.replacements.repairAllLines)

    const btnCls = (isActive) => "controls__button" + (isActive ? " controls__button--active" : "")

    const clickChangeParagraphStyle = mkExecAndFocus((ev) =>
        actions.paragraph.setCurrentParagraphStyle(ev.target.value),
    )

    function Controls() {
        const [editorState] = useEditorState(editor, actions)
        const [undoState] = useUndoManager(yUndoManager)

        return html`<${Fragment}>
            <button
                disabled=${!undoState.canUndo}
                class="controls__button"
                id="controls__undo"
                title="Rückgängig"
                onClick=${undo}
            >
                ←
            </button>
            <button
                disabled=${!undoState.canRedo}
                class="controls__button"
                id="controls__redo"
                title="Wiederholen"
                onClick=${redo}
            >
                →
            </button>

            <span>|</span>

            <button
                class=${btnCls(editorState.isBold)}
                id="controls__bold"
                title="Fett"
                style="font-weight: bold"
                onClick=${inlineActions.toggleBold}
            >
                F
            </button>
            <button
                class=${btnCls(editorState.isItalic)}
                id="controls__italic"
                title="Kursiv"
                style="font-style: italic"
                onClick=${inlineActions.toggleItalic}
            >
                K
            </button>
            <button
                class=${btnCls(editorState.isStrikethrough)}
                id="controls__strikethrough"
                title="Durchgestrichen"
                style="text-decoration: line-through"
                onClick=${inlineActions.toggleStrikethrough}
            >
                D
            </button>
            <button
                class=${btnCls(editorState.isMono)}
                id="controls__mono"
                title="Monospaced"
                style="font-family: 'Courier New', monospace"
                onClick=${inlineActions.toggleMono}
            >
                m
            </button>

            <span>|</span>

            <button
                class="controls__button"
                id="controls__inc"
                title="Einzug erhöhen"
                onClick=${tabInc}
            >
                »
            </button>
            <button
                class="controls__button"
                id="controls__dec"
                title="Einzug verringern"
                onClick=${tabDec}
            >
                «
            </button>

            <span>|</span>

            <span>
                <label for="controls__paragraph">Zeilenformat: </label>
                <select
                    name="controls__paragraph"
                    id="controls__paragraph"
                    value=${editorState.paragraphStyle}
                    onChange=${clickChangeParagraphStyle}
                >
                    <option value="p">Absatz</option>
                    <option value="ul">Stichpunkte-Liste</option>
                    <option value="ol">Numerierte Liste</option>
                    <option value="task">Aufgabenliste</option>
                    <option value="h1">Überschrift Ebene 1</option>
                    <option value="h2">Überschrift Ebene 2</option>
                    <option value="h3">Überschrift Ebene 3</option>
                    <option value="h4">Überschrift Ebene 4</option>
                    <option value="h5">Überschrift Ebene 5</option>
                    <option value="h6">Überschrift Ebene 6</option>
                </select>
            </span>

            <span>|</span>

            <button
                disabled=${editorState.isTable}
                class=${btnCls(editorState.isTable)}
                id="controls__table"
                title="Tabelle einfügen"
                onClick=${tableActions.insertTable}
            >
                Tabelle
            </button>
            <button
                disabled=${!editorState.isTable}
                class="controls__button"
                id="controls__addrow"
                title="Tabellenzeile einfügen"
                onClick=${tableActions.addRow}
            >
                +Z
            </button>
            <button
                disabled=${!editorState.isTable}
                class="controls__button"
                id="controls__addcol"
                title="Tabellenspalte einfügen"
                onClick=${tableActions.addCol}
            >
                +S
            </button>

            <span>|</span>

            <button
                disabled=${editorState.isLink}
                class=${btnCls(editorState.isLink)}
                id="controls__hyperlink"
                title="Link einfügen"
                onClick=${actions.insert.hyperlinkDialog}
            >
                Link
            </button>

            <span class="controls__flex-break"></span>

            <button
                class="controls__button"
                id="controls__internal"
                title="Markierte Zeilen intern schalten"
                onClick=${internal}
            >
                INTERN
            </button>
            <button
                class="controls__button"
                id="controls__comment"
                title="Markierte Zeilen im Endprodukt ausblenden"
                onClick=${comment}
            >
                KOMMENTAR
            </button>
            <button
                class="controls__button"
                id="controls__resolution"
                title="Markierte Zeilen sind ein Beschluss"
                onClick=${resolution}
            >
                BESCHLUSS
            </button>
            <button
                class="controls__button"
                id="controls__act"
                title="Markierte Zeilen als Aufgabe markieren"
                onClick=${act}
            >
                ACT
            </button>
            <button
                class="controls__button"
                id="controls__meta"
                title="Sitzungsinformationen am Anfang des Protokolls"
                onClick=${meta}
            >
                Meta
            </button>

            <span>|</span>

            <button
                class="controls__button"
                id="controls__repair"
                title="Häufige fehler reparieren"
                onClick=${repair}
            >
                Shame
            </button>
        <//>`
    }
    return Controls
}
