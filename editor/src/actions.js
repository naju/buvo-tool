/**
 * This file contains code mirror actions.
 */

import { createStyleToggler } from "hypermd/keymap/hypermd"
import { repeatStr } from "hypermd/core/utils"

import { fixCommonMistakes } from "./helpers"

/**
 * @param {import('codemirror').Editor} editor
 * @returns a structure of the actions defined here
 */
export default function createEditorActions(editor) {
    // #region utilities
    /******************************************* utils ********************************************/

    /**
     * Tests if a text is contained in an Array
     * @param {any[]} array Array of elements that may contain text
     * @param {String} text text to search for
     * @returns {Boolean}
     */
    function textInArray(array, text) {
        for (let index = 0; index < array.length; index++) {
            const element = array[index]
            if (typeof element == "string" && element.indexOf(text) != -1) {
                return true
            }
        }
        return false
    }

    /**
     * Search for the n-th occurence of a character
     * @param {String} char character to search for
     * @param {Number} num Character occurence count until match
     * @param {String} str String where the search will happen
     * @returns {Number} the position of the n-th occurence of `char`
     */
    function nthCharPosition(char, num, str) {
        let index = 0
        for (let counter = 0; counter < num && index < str.length; index++) {
            if (str[index] == char) {
                counter++
            }
        }
        return index
    }

    /**
     * insert text after a character occured `num` times in a line current editor
     * @param {String} char character to count
     * @param {Number} num number of accurences of `char` after which the insertion wil be made
     * @param {String} insertion text to insert
     * @param {Number} line line number
     */
    function insertAfterNthCharOccurence(char, num, insertion, line) {
        let lineContent = editor.getLine(line)
        let ch = nthCharPosition(char, num, lineContent)
        editor.replaceRange(insertion, { line, ch })
    }

    /**
     * Assert if the anchor is before (or the same as) the head
     * @param {import("codemirror").Position} anchor
     * @param {import("codemirror").Position} head
     * @returns {Boolean}
     */
    function anchorIsBeforeHead(anchor, head) {
        return anchor.line < head.line || (anchor.line == head.line && anchor.ch <= head.ch)
    }

    const mdBlockFormatExp = /^(#+|((\s*>?)*([-+*]( \[[ x]?\])?|\d+\.)?))\s+/
    /**
     * Search for the md block format end of a given line
     * @param {String} line the line to search for mardown block formats
     * @returns {Number} where the md block format ends
     */
    const mdStrFormatEnd = (line) => line.match(mdBlockFormatExp)[0].length

    const lineStart = (pos) => ({ line: pos.line, ch: 0 })
    const lineEnd = (pos) => ({
        line: pos.line,
        ch: editor.getLine(pos.line).length,
    })
    const mdFormatEnd = (pos) => ({
        line: pos.line,
        ch: mdStrFormatEnd(editor.getLine(pos.line)),
    })

    /**
     * Extend the given selections to the full lines.
     * @param {Optional<import("codemirror").Range[]>} selections selections to extend.
     *        defaults to current editor selections.
     */
    function extendSelectionsToFullLine(selections = null) {
        selections = selections ?? editor.listSelections()
        selections = selections.map(({ anchor, head }) =>
            anchorIsBeforeHead(anchor, head)
                ? { anchor: lineStart(anchor), head: lineEnd(head) }
                : { anchor: lineEnd(anchor), head: lineStart(head) },
        )
        editor.setSelections(selections)
    }

    function extendSelectionsToMarkdownFormatStart(selections = null) {
        selections = selections ?? editor.listSelections()
        selections = selections.map(({ anchor, head }) =>
            anchorIsBeforeHead(anchor, head)
                ? { anchor: mdFormatEnd(anchor), head: lineEnd(head) }
                : { anchor: lineEnd(anchor), head: mdFormatEnd(head) },
        )
        editor.setSelections(selections)
    }

    // #endregion utilities

    // #region togglers
    /****************************************** togglers ******************************************/

    const _toggleBold = createStyleToggler(
        (state) => state.strong,
        (token) => / formatting-strong /.test(token.type),
        (state) => repeatStr((state && state.strong) || "*", 2), // ** or __
    )

    function toggleBold() {
        _toggleBold(editor)
    }

    const _toggleItalic = createStyleToggler(
        (state) => state.em,
        (token) => / formatting-em /.test(token.type),
        (state) => (state && state.em) || "*",
    )

    function toggleItalic() {
        _toggleItalic(editor)
    }

    const _toggleMono = createStyleToggler(
        (state) => state.code,
        (token) => / formatting-code /.test(token.type),
        (state) => "`",
    )

    function toggleMono() {
        _toggleMono(editor)
    }

    const _toggleStrikethrough = createStyleToggler(
        (state) => state.strikethrough,
        (token) => / formatting-strikethrough /.test(token.type),
        (state) => "~~",
    )

    function toggleStrikethrough() {
        _toggleStrikethrough(editor)
    }
    // #endregion togglers

    // #region paragraph styles
    /************************************** paragraph styles **************************************/

    /**
     * @param {import("codemirror").Position} cursor
     * @returns {String} one of h1 - h6, ol, task, ul or p
     */
    function getParagraphStyleAt(cursor) {
        let lineState = editor.getStateAfter(cursor.line)
        let lineInfo = editor.lineInfo(cursor.line)

        let styles = lineInfo.handle.styles
        if (!styles) throw new Error("no styles")

        if (textInArray(styles, "formatting-header")) {
            return "h" + lineState.header
        } else if (textInArray(styles, "formatting-list-ol")) {
            return "ol"
        } else if (textInArray(styles, "formatting-task")) {
            return "task"
        } else if (textInArray(styles, "formatting-list-ul")) {
            return "ul"
        } else {
            return "p"
        }
    }

    /**
     * @returns {String} @see getParagraphStyleAt
     */
    function getCurrentParagraphStyle() {
        return getParagraphStyleAt(editor.getCursor())
    }

    const ulExp = /[ \t]*[+*-] (\[[x ]?\] +)?/
    function removeUlFormat(line) {
        return line.replace(ulExp, "")
    }

    const olExp = /[ \t]*\d+\.[ \t]+/
    function removeOlFormat(line) {
        return line.replace(olExp, "")
    }

    const hExp = /^(?<level>#{1,6}) +(?<txt>.*?)( +\k<level>)?$/
    function removeHFormat(line) {
        return line.match(hExp)?.groups["txt"]
    }

    function removeParagraphStyleFromLine(text, paragraphStyle) {
        switch (paragraphStyle) {
            case "ol":
                return removeOlFormat(text)
            case "ul":
                return removeUlFormat(text)
            case "task":
                return removeUlFormat(text)
            case "h1":
            case "h2":
            case "h3":
            case "h4":
            case "h5":
            case "h6":
                return removeHFormat(text)
            default:
                return text
        }
    }

    function addParagraphStyleToLine(text, paragraphStyle) {
        switch (paragraphStyle) {
            case "ul":
                return "- " + text
            case "ol":
                return "1. " + text
            case "task":
                return "- [ ] " + text
            case "p":
                return text
            default:
                let level = +(paragraphStyle.match(/h(?<level>\d)/)?.groups["level"] || 1)
                return repeatStr("#", level) + " " + text
        }
    }

    /**
     * Chages the style of the current paragraph
     * @param {import("codemirror").Editor} editor
     * @param {String} newStyle one of ol, ul, task, p or h1 - h6
     */
    function setCurrentParagraphStyle(newStyle) {
        let cursor = editor.getCursor()
        let lineInfo = editor.lineInfo(cursor.line)

        let text = lineInfo.text
        let oldText = text
        let oldLength = text.length

        let paragraphStyle = getParagraphStyleAt(cursor)
        text = removeParagraphStyleFromLine(text, paragraphStyle)
        text = addParagraphStyleToLine(text, newStyle)

        editor.replaceRange(
            text,
            { line: cursor.line, ch: 0 },
            { line: cursor.line, ch: oldLength },
            oldText,
        )
    }
    // #endregion paragraph styles

    // #region insertions
    /***************************************** insertions *****************************************/

    /**
     * Insert a hyperlink at the current position
     * @param {import("codemirror").Editor} editor
     * @param {String} address hyperlink address
     */
    function insertHyperlink(address) {
        if (!address.match(/^[a-zA-Z]+:/)) {
            // guess the protocol...
            address = "https://" + address
        }
        let cursor = editor.getCursor()
        editor.operation(() => {
            editor.replaceRange(`[](${address})`, cursor)
            editor.setCursor(cursor.line, cursor.ch + 1)
        })
    }

    /**
     * Open a dialog to insert a hyperlink.
     */
    function hyperlinkDialog() {
        editor.openDialog(
            `<div class="cm__hyperlink">
            <label for="link">Adresse:</label>&nbsp;
            <input name="link" />
        </div>`,
            insertHyperlink,
        )
    }

    /**
     * Inserts each of the current selection in a given template.
     * @param {import("codemirror").Range} selection the selection to insert text around
     * @param {String} beforeCursor text to insert before the cursor
     * @param {String} afterCursor text to insert after the cursor
     * @param {Boolean} fullLine is the complete line should be selected
     */
    function insertAround({ anchor, head }, beforeSelection, afterSelection = "") {
        editor.operation(() => {
            if (anchorIsBeforeHead(anchor, head)) {
                editor.replaceRange(afterSelection, head)
                editor.replaceRange(beforeSelection, anchor)
            } else {
                editor.replaceRange(afterSelection, anchor)
                editor.replaceRange(beforeSelection, head)
            }
            // editor.setCursor()
        })
    }

    /**
     * insert given strings around the current selection
     * @param {String} beforeSelection string to insert before the selection(s)
     * @param {String} afterSelection string to insert after the selection(s)
     */
    function insertAroundCurrentSelections(beforeSelection, afterSelection = "") {
        editor.operation(() => {
            let selections = editor.listSelections()
            for (let index = selections.length - 1; index >= 0; index--) {
                const selection = selections[index];
                insertAround(selection, beforeSelection, afterSelection)
            }
        })
    }

    /**
     * insert given strings around the currently selected lines
     * @param {String} beforeSelection string to insert before the selection(s)
     * @param {String} afterSelection string to insert after the selection(s)
     */
    function insertAroundCurrentlySelectedLines(beforeSelection, afterSelection = "") {
        editor.operation(() => {
            extendSelectionsToFullLine(editor.listSelections())
            insertAroundCurrentSelections(beforeSelection, afterSelection)
        })
    }

    /**
     * add a new line with the same paragraph style
     * and place the cursor between the inserted strings
     * @param {String} beforeCursor string to insert before the cursor
     * @param {String} afterCursor string to insert after the cursor
     */
    function addNewParagraphWithSameStyle(beforeCursor, afterCursor = "") {
        editor.operation(() => {
            let cursor = editor.getCursor()
            let lineEnd = { line: cursor.line, ch: editor.getLine(cursor.line).length }
            let pStyle = getParagraphStyleAt(cursor)
            editor.replaceRange("\n", lineEnd)
            let newLine = { line: cursor.line + 1, ch: 0 }
            editor.setCursor(newLine)
            let beforeCursorWithStyle = addParagraphStyleToLine(beforeCursor, pStyle)
            insertAround({ anchor: newLine, head: newLine }, beforeCursorWithStyle, afterCursor)
            editor.setCursor({ line: newLine.line, ch: beforeCursorWithStyle.length })
        })
    }

    // #endregion insertions

    // #region table tools
    /******************************************* tables *******************************************/

    /**
     * Create a table row string with a certain number of columns
     * @param {Number} cols number of columns
     * @param {String} inner character to fill the space between the coumns with
     * @returns {String} a table row
     */
    function makeRow(cols, inner = " ") {
        let row = "\n|"
        for (let col = 0; col < cols; col++) {
            row += repeatStr(inner, 6) + "|"
        }
        return row
    }

    /**
     * Create a github markdown style table
     * @param {Number} rows number of rows
     * @param {Number} cols number of columns
     * @returns {String} a github markdown style table
     */
    function makeTable(rows, cols) {
        let table = "\n"
        // table header
        table += makeRow(cols)
        // formatting line
        table += makeRow(cols, "-")
        // rows
        for (let row = 0; row < rows; row++) {
            table += makeRow(cols)
        }
        table += "\n\n"
        return table
    }

    /**
     * Insert a github markdown style table after the line where the current cursor is.
     */
    function insertTable() {
        let cursor = editor.getCursor()
        let line = editor.getLine(cursor.line)
        editor.replaceRange(makeTable(2, 3), {
            line: cursor.line,
            ch: line.length,
        })
    }

    /**
     * Add a table row to the currently selected table in the editor
     */
    function addRow() {
        let cursor = editor.getCursor()
        let line = editor.getLine(cursor.line)
        let prevLine = cursor.line > 0 && editor.getLine(cursor.line - 1)
        let pipes = line.match(/\|/g).length
        if (prevLine && prevLine.startsWith("|") && line.startsWith("|")) {
            let cols = pipes - 1
            editor.replaceRange(makeRow(cols), {
                line: cursor.line,
                ch: line.length,
            })
        }
    }

    /**
     * Test if a line is inside a table
     * @param {Number} lineNumber line number in editor
     * @returns {Boolean} if the line is inside a table
     */
    function isTableLine(lineNumber) {
        return editor.getLine(lineNumber).startsWith("|")
    }

    /**
     * Add a table column to the currently selected table in the editor
     */
    function addCol() {
        const cursor = editor.getCursor()
        let line = cursor.line
        const afterPipe = editor.getLine(cursor.line).slice(0, cursor.ch).match(/\|/g)?.length || 1
        if (!isTableLine(line)) {
            return
        }
        while (line - 1 >= 0 && isTableLine(line - 1)) {
            line--
        }
        const normalCol = repeatStr(" ", 6) + "|"
        const formatCol = repeatStr("-", 6) + "|"
        editor.operation(() => {
            insertAfterNthCharOccurence("|", afterPipe, normalCol, line)
            line++
            insertAfterNthCharOccurence("|", afterPipe, formatCol, line)
            line++
            while (isTableLine(line)) {
                insertAfterNthCharOccurence("|", afterPipe, normalCol, line)
                line++
            }
        })
    }
    // #endregion table tools

    // #region replacements
    /**************************************** replacements ****************************************/

    function repairAllLines() {
        editor.operation(() => {
            editor.eachLine((handle) => {
                let line = handle.lineNo()
                editor.replaceRange(fixCommonMistakes(handle.text), { line, ch: 0 }, { line })
            })
        })
    }

    // #endregion replacements

    return {
        editor,
        utils: {
            textInArray,
            nthCharPosition,
            insertAfterNthCharOccurence,
            extendSelectionsToFullLine,
            extendSelectionsToMarkdownFormatStart,
        },
        inline: { toggleBold, toggleItalic, toggleMono, toggleStrikethrough },
        table: { insertTable, addCol, addRow, makeTable },
        insert: {
            insertTable,
            insertHyperlink,
            hyperlinkDialog,
            insertAroundCurrentSelections,
            insertAroundCurrentlySelectedLines,
            addNewParagraphWithSameStyle,
        },
        paragraph: {
            getParagraphStyleAt,
            getCurrentParagraphStyle,
            setCurrentParagraphStyle,
        },
        replacements: { repairAllLines },
    }
}
