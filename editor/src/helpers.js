/**
 * download a string as a text file
 * from https://stackoverflow.com/questions/45831191/generate-and-download-file-from-js
 * @param {string} filename filename to save to
 * @param {string} text text to save
 */
export function download(filename, text) {
	var element = document.createElement("a")
	element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text))
	element.setAttribute("download", filename)

	element.style.display = "none"
	document.body.appendChild(element)

	element.click()

	document.body.removeChild(element)
}

/**
 * Cookie function
 * https://docs.djangoproject.com/en/3.2/ref/csrf/
 * @param {string} name cookie name
 * @returns {string} cookie value
 */
export function getCookie(name) {
	let cookieValue = null
	if (document.cookie && document.cookie !== "") {
		const cookies = document.cookie.split(";")
		for (let i = 0; i < cookies.length; i++) {
			const cookie = cookies[i].trim()
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === name + "=") {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1))
				break
			}
		}
	}
	return cookieValue
}

/**
 * fixes a line of text
 * @param {string} line the line to fix
 * @returns the fixed line
 */
export function fixCommonMistakes(line /* fixDates = false */) {
	line = line
		// .replace(/(?<!^)\s+)/g, " ") // collapse whitespace if it is not at line start (no lookaround in safari :/)
		.replace(/(\S)([^\S\r\n]+)(\S)/g, "$1 $3") // collapse whitespace if it is not at line start
		.replace(/\s+$/gm, "") // trim trailing whitespace
		.replace(/^(?<d>[\d]{1,2})\\?\.(?<f> ?[+-—/] ?)?(?<m>[\d]{1,2})/gm, "- $<d>.$<f>$<m>") // fuuuuuck
		.replace(/[·•]/g, "-")
		// .replace(/(?<=\S) - /g, " – ") // fix dashes (no lookaround in safari :/)
		.replace(/(\S) - /g, "$1 – ") // fix dashes
		.replace(/^\\(?<f>[-+*]) /gm, "$<f> ") // fix lists
		.replace(/(\w)\\?\*(\w)/g, "$1:$2") // fix gender asterik
		.replace(/-{1,2}>/g, "→")
		.replace(/<-{1,2}/g, "←")
		.replace(/(?<f>[*_]{1,2}):/g, ":$<f>") // fix colon after format
		.replace(/^\*{2}(.*?):?\*{2}$/gm, "#### $1") // replace "headings"

	// #region fix date format
	// DISABLE. TOO BUGGY. Also Safari does not support lookarounds and I don't want to update the regex.
	// TODO: update regex or write custom parser
	// if (fixDates) {
	// 	let newLine = ""
	// 	let oldLine = line
	// 	const dateRegExp = /(?<d>\d{1,2})\.(?<m>\d{1,2})(\.)?(?<y>(?<=\.)\d{2,4})?/
	// 	/** @type {RegExpMatchArray} */ let match
	// 	while ((match = oldLine.match(dateRegExp))) {
	// 		let date = match.groups
	// 		if (date.y?.length == 2) {
	// 			date.y = (+date.y < 60 ? "20" : "19") + date.y
	// 		} else if (!date) {
	// 			date.y = ""
	// 		}
	// 		date.y = (date.y?.length == 2 ? "20" + date.y : date.y) || ""
	// 		let dateStr = `${date.d.padStart(2, "0")}.${date.m.padStart(2, "0")}.${date.y}`
	// 		let endIdx = match.index + match[0].length
	// 		newLine += oldLine.slice(0, endIdx).replace(match[0], dateStr)
	// 		oldLine = oldLine.slice(endIdx)
	// 	}
	// 	newLine += oldLine
	// 	return newLine || line
	// }
	// #endregion fix date format

	return line
}

/**
 * Assign a value to an object key and return the object.
 *
 * Useful for reducer functions.
 *
 * @param {Object} o the object that will be assigned to
 * @param {String | Symbol} attr the object key
 * @param {Any} val the object value
 * @returns
 */
export function assignAndReturn(o, attr, val) {
	o[attr] = val
	return o
}
