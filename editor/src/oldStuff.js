async function bytesToHexdigest(bytes) {
	// taken from MDN: https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest
	const hashBuffer = await crypto.subtle.digest("SHA-256", bytes)
	const hashArray = Array.from(new Uint8Array(hashBuffer))
	const hashHex = hashArray.map((b) => b.toString(16).padStart(2, "0")).join("")
	return hashHex
}

async function onPersistMessage(message) {
	switch (message.type) {
		case "send_blob":
			let doc = Y.encodeStateAsUpdate(ydoc)
			let digest = await bytesToHexdigest(doc)
			if (digest == message.digest) {
				persistanceClient.send({
					type: "crdt_blob_did_not_change",
					topic: settings.room,
					digest,
				})
			} else {
				persistanceClient.send({
					type: "save_crdt_blob",
					topic: settings.room,
					blob: toBase64(doc),
				})
			}
			break
	}
}
persistanceClient.on("message", onPersistMessage)

persistanceClient.on("connect", () => {
	persistanceClient.send({
		type: "subscribe",
		topics: [settings.room],
	})
})
