# Editor To Do

- [ ] Language Tool integration (see https://github.com/Clemens-E/obsidian-languagetool-plugin)
    - [ ] with custom LT deployment

- [ ] image upload and editor integration

- [x] Buttons für
    - [x] ACT, BESCHLUSS (fügt auch template für Ergebnis ein)
    - [x] markiertes intern schalten
- [x] Wikiseiten im Editor verlinken
- [ ] ASCII Tabelle ordentlich formatieren
- [x] Workflow zum beheben üblicher Fehler
- [x] Paste aus Word fixen (zB bullet points)
- [-] Pfeile automatisch ersetzen
- [ ] evtl Anführungszeichen automatisch ersetzen (macht pandoc das?)
- [ ] zweiter Klick auf INTERN/KOMMENTAR/etc entfernt die Markierung wieder
