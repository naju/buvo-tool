# to do list

## Bugfixes

* [x] fix bug where saving without uploading a new protocol shows the following error:

  > Das Start-Datum am Beginn der Protokolldatei konnte nicht verarbeitet
  > werden. Bitte überprüfe, ob es ein korrektes Format hat (TT.MM.JJJJ).

## Data loss prevention

* [x] save blob on protocol save
  * [x] delete the blob when a protocol is uploaded
  * [x] Test the feature
* [x] save unknown metadata keys to the json field

## Requested Features

* [x] generate appendix (images, PDF)
* [x] download new protocol as md
* [x] add how to click a link to tips
* [x] only superuser: manage crdt blobs
* [ ] show active rooms (=stored blobs) to normal users
* [x] add exit without saving link to editor
* [ ] Allow custom editor links for usage as "Pad"
* [x] Buttons: aktives Format dunkel hinterlegen

## Compatibility

* [ ] safari support

## Nice to have

* [x] editor shortcut help
* [ ] filter: meine aufgaben
* [ ] ggf custom parser
* [ ] support POST-Script-compatible attachments (PDF, EPS, …)
