#!/bin/bash
set -euo pipefail

# these may be exposed as a volume but they need to be writeable.
test -d /srv/data && chown -R buvotool:buvotool /srv/data
test -d /srv/static_copy && chown -R buvotool:buvotool /srv/static_copy

# if the mails volume is there but it's new, copy the example files into the volume.
if ! [ -d /srv/mails/tasks ]; then
	mkdir -p /srv/mails/tasks
	cp /srv/tasks/templates/mails/* /srv/mails/tasks/
fi

if [[ "$1" == "manage" ]]; then
	# management commands for "docker run --rm buvo-tool manage ..."
	shift
	exec gosu buvotool:buvotool <<-EOF
		set -euo pipefail
		python3 manage.py $@
	EOF
else
	# drop privileges to run the server
	exec gosu buvotool:buvotool bash <<- EOF
		set -euo pipefail
		python3 manage.py collectstatic --noinput
		python3 manage.py migrate
		# exec gunicorn -k buvo.uvicorn.UvicornWorker buvo.asgi $@
		exec uvicorn buvo.asgi:application --host=0.0.0.0 $@
	EOF
fi
